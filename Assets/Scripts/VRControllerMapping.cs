﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class VRControllerMapping : MonoBehaviour {
    public SteamVR_Input_Sources handType;
    public SteamVR_Action_Boolean triggerAction;
    public UnrealFPS.ShootingScript shootingScript;
    public Transform attackPoint;

    // variable to prevent Trigger holding and multiple shots
    [SerializeField]
    private TestTracker tracker;
    private bool wasReleased = true;

    void Start ( ) {
        tracker = TestTracker.Instance ( );
    }

    void Update ( ) {
        if ( !tracker ) tracker = TestTracker.Instance ( );
        if ( triggerAction.GetState ( handType ) && wasReleased ) {
            wasReleased = false;
            if ( tracker.isMenuOpen ( ) ) {
                tracker.closeMenu ( );
            } else {
                shootingScript.Shoot ( );
            }
        } else {
            wasReleased = true;
        }

        Debug.DrawRay ( attackPoint.position, attackPoint.forward );
    }

}
