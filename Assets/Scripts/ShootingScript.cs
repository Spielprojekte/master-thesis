﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnrealFPS.UI;
using UnrealFPS.Utility;

namespace UnrealFPS {
    /// <summary>
    /// Weapon attack type
    ///     Description:
    ///         RayCast - Attack by ray.
    ///         Physics - Attack by physics (Rigidbody).
    ///         Throw   - Attack by physics (Rigidbody).
    /// </summary>
    public enum AttackType {
        RayCast,
        Physics

    }

    /// <summary>
    /// Weapon shoot type
    ///     Description:
    ///         Default - Shoot type like pistol/rifle with a one bullet.
    ///         Fractional - Shoot type like shootgun with a fraction.
    /// </summary>
    public enum ShootType {
        Default,
        Fractional

    }

    [System.Serializable]
    public struct FireEffects {
        public ParticleSystem muzzleFlash;
        public ParticleSystem cartridgeEjection;
    }

    public class ShootingScript : MonoBehaviour {

        [SerializeField] private AttackType attackType;
        [SerializeField] private Transform attackPoint;
        [SerializeField] private PhysicsBullet bullet;
        //For Physics/Throw Attack
        [SerializeField] private RayBullet rayBullet;
        //For RayCast Attack
        [SerializeField] private float delay;
        [SerializeField] private float attackImpulse;
        [SerializeField] private float attackRange;
        [SerializeField] private AudioClip attackSound;
        [SerializeField] private AudioClip emptySound;
        [SerializeField] private FireEffects fireEffects;
        [SerializeField] private SpreadSystem spreadSystem = new SpreadSystem ( );

        private AudioSource audioSource;
        private PhysicsBullet physicsBullet;

        private bool isAttack;
        private bool triggerWasReleased;
        private float s_Delay;

        /// <summary>
        /// Awake is called when the script instance is being loaded.
        /// </summary>
        /// 
        /// <remarks>
        /// Awake is called only once during the lifetime of the script instance.
        /// Awake is always called before any Start functions.
        /// This allows you to order initialization of scripts.
        /// </remarks>
        protected virtual void Awake ( ) {
            audioSource = GetComponent<AudioSource> ( );
            if ( attackType == AttackType.Physics ) physicsBullet = bullet.GetComponent<PhysicsBullet> ( );
            s_Delay = delay;
        }

        /// <summary>
        /// Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        protected virtual void Update ( ) {
            AttackBehaviour ( );
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void AttackBehaviour ( ) {
            if ( Input.GetAxis ( "Fire" ) == 0 ) triggerWasReleased = true;
            if ( ( Input.GetButtonDown ( "Fire" ) && !isAttack ) || ( Input.GetAxis ( "Fire" ) != 0 && triggerWasReleased ) ) {
                triggerWasReleased = false;
                Debug.Log ( "Fired" );
                spreadSystem.BulletSpreadProcessing ( );
                if ( attackSound != null ) audioSource.PlayOneShot ( attackSound );
                switch ( attackType ) {
                case AttackType.RayCast:
                    RayCastAttack ( );
                    break;
                case AttackType.Physics:
                    PhysicsAttack ( );
                    break;
                }
                AttackParticleEffect ( );
                spreadSystem.CameraSpreadProcessing ( );
                isAttack = true;
            } else if ( Input.GetButtonDown ( "Fire" ) && !isAttack ) {
                if ( emptySound != null ) audioSource.PlayOneShot ( emptySound );
                isAttack = true;
            }
            Delay ( );
        }

        public void Shoot ( ) {
            if ( !isAttack ) {
                spreadSystem.BulletSpreadProcessing ( );
                if ( attackSound != null ) audioSource.PlayOneShot ( attackSound );
                switch ( attackType ) {
                case AttackType.RayCast:
                    RayCastAttack ( );
                    break;
                case AttackType.Physics:
                    PhysicsAttack ( );
                    break;
                }
                AttackParticleEffect ( );
                spreadSystem.CameraSpreadProcessing ( );
                isAttack = true;
                Delay ( );
            }
        }

        protected virtual void RayCastAttack ( ) {
            RaycastHit raycastHit;
            //if ( Physics.Raycast ( attackPoint.position, attackPoint.forward, out raycastHit, attackRange, LayerMask.NameToLayer ( "Targets" ) ) ) {
            if ( Physics.Raycast ( attackPoint.position, attackPoint.forward, out raycastHit, attackRange ) ) {

                Decal.Instantiate ( rayBullet.BulletHitEffects, raycastHit, audioSource );
                TestTracker.Instance ( ).addRecord ( raycastHit );
                
                Debug.Log ( raycastHit.transform.gameObject.name );
            }
        }

        protected virtual void PhysicsAttack ( ) {
            for ( int i = 0; i < physicsBullet.NumberBullet; i++ ) {
                if ( physicsBullet.NumberBullet > 1 ) attackPoint.localRotation = Quaternion.Euler ( Random.Range ( -physicsBullet.Variance, physicsBullet.Variance ), Random.Range ( -physicsBullet.Variance, physicsBullet.Variance ), 0 );
                PhysicsBullet bulletClone = UPoolManager.Instance.PopOrCreate<PhysicsBullet> ( bullet, attackPoint.position, attackPoint.rotation );
                bulletClone.GetComponent<Rigidbody> ( ).AddForce ( attackPoint.forward * attackImpulse, ForceMode.Impulse );
            }
            if ( physicsBullet.NumberBullet > 1 ) attackPoint.localRotation = Quaternion.identity;
        }

        /// <summary>
        /// Attack delay.
        /// </summary>
        protected virtual void Delay ( ) {
            if ( isAttack ) {
                delay -= Time.deltaTime;
                if ( delay <= 0 ) {
                    isAttack = false;
                    delay = s_Delay;
                }
            }
        }

        /// <summary>
        /// Send damage from attack.
        /// </summary>
        /// <param name="raycastHit"></param>
        /// <param name="damage"></param>
        protected virtual void SendDamage ( RaycastHit raycastHit, int damage ) {             

        }

        /// <summary>
        /// Play attack particle effect.
        /// </summary>
        /// 
        /// <remarks>
        /// Played effect when weapon attack.
        /// </remarks>
        protected virtual void AttackParticleEffect ( ) {
            if ( fireEffects.muzzleFlash != null ) fireEffects.muzzleFlash.Play ( );
            if ( fireEffects.cartridgeEjection != null ) fireEffects.cartridgeEjection.Play ( );
        }
    }
}