﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/*
 * 
 */
public class Testuser : MonoBehaviour {

    public GameObject testDoneMsg;

    private const string logPathRoot = "Assets/Resources/Logfiles/";
    private const string pathOfuserFile = logPathRoot + "users.csv";
    private string userEntry = "";

    private void Start ( ) {
        if ( !PlayerPrefs.HasKey ( "userNum" ) ) PlayerPrefs.SetInt ( "userNum", 0 ); 
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        checkFileDirectory ( );
        //init ( );
    }

    private void init ( ) {
        PlayerPrefs.SetInt ( "userNum", 0 ); 
        clearTestLog ( );
    }

    private void clearTestLog ( ) {
        for ( int i = 0; i < 13; i++ ) {
            PlayerPrefs.SetInt ( "scene" + i, 0 );
        } 
    }

    /*
     * checking if the needed directory exists
     * if not creates paths
     */
    private void checkFileDirectory ( ) {
        if ( System.IO.Directory.Exists ( "Assets/Resources/Logfiles/sr" ) == false ) {
            System.IO.Directory.CreateDirectory ( "Assets/Resources/Logfiles/" );
        }
        if ( System.IO.Directory.Exists ( "Assets/Resources/Logfiles/fl" ) == false ) {
            System.IO.Directory.CreateDirectory ( "Assets/Resources/Logfiles/" );
        }
    }

    //PUBLIC FUNCTIONS

    public void addUserDataFromDropdownsAndSliders ( ) {
        Dropdown [] dropdowns = Transform.FindObjectsOfType<Dropdown> ( );
        foreach ( Dropdown dp in dropdowns ) {
            userEntry += dp.options [ dp.value ].text + ";";
        }

        Slider [] sliders = Transform.FindObjectsOfType<Slider> ( );
        foreach ( Slider sl in sliders ) {
            userEntry += sl.value + ";";
        }
    }

    public void addFPSPreference ( String pref ) {
        userEntry += pref + ";";
    }

    public void writeUserEntryToFile ( ) {
        // User file is separate from each test run
        System.IO.StreamWriter file = new System.IO.StreamWriter ( pathOfuserFile, true );
        file.WriteLine ( PlayerPrefs.GetInt ( "userNum" ) + ";" + userEntry );
        file.Close ( );
        clearTestLog ( );
    }

    public void setLogPath ( int sceneNum ) {
        PlayerPrefs.SetString ( "path", logPathRoot + "sr/" + PlayerPrefs.GetInt ( "userNum" ) + "_" + sceneNum + PlayerPrefs.GetString ( "testing" ) + ".csv" );
    }

    public void raiseUserCounter ( ) {
        PlayerPrefs.SetInt ( "userNum", PlayerPrefs.GetInt ( "userNum" ) + 1 );
    }

    public void decreaseUserCounter ( ) {
        PlayerPrefs.SetInt ( "userNum", PlayerPrefs.GetInt ( "userNum" ) - 1 );
        
    }

    public void chooseAndStartNextScenario ( ) {
        PlayerPrefs.SetString ( "testing", "" );
        if ( checkForTestEnd ( ) ) {
            int random = Mathf.RoundToInt ( UnityEngine.Random.Range ( 0.5f, 13.5f ) - 0.5f );
            if ( random == 13 ) random = 0;
            int keyvalue = PlayerPrefs.GetInt ( "scene" + random );
            while ( keyvalue != 0 ) {
                random = Mathf.RoundToInt ( UnityEngine.Random.Range ( 0.5f, 13.5f ) - 0.5f );
                if ( random == 13 ) random = 0;
                keyvalue = PlayerPrefs.GetInt ( "scene" + random );            
            }
            PlayerPrefs.SetInt ( "scene" + random, 1 );     

            switch ( random ) {
            case 0: 
                PlayerPrefs.SetFloat ( "target_scale", 0.075f );
                PlayerPrefs.SetFloat ( "target_dist", 7.5f );
                PlayerPrefs.SetString ( "mode", "headmouse" );
                break;
            case 1:
                PlayerPrefs.SetFloat ( "target_scale", 0.025f );
                PlayerPrefs.SetFloat ( "target_dist", 7.5f );
                PlayerPrefs.SetString ( "mode", "headmouse" );
                break;
            case 2:
                PlayerPrefs.SetFloat ( "target_scale", 0.025f );
                PlayerPrefs.SetFloat ( "target_dist", 20f );
                PlayerPrefs.SetString ( "mode", "headmouse" );
                break;
            case 3:
                PlayerPrefs.SetFloat ( "target_scale", 0.075f );
                PlayerPrefs.SetFloat ( "target_dist", 7.5f );
                PlayerPrefs.SetString ( "mode", "mouse" );
                break;
            case 4:
                PlayerPrefs.SetFloat ( "target_scale", 0.025f );
                PlayerPrefs.SetFloat ( "target_dist", 7.5f );
                PlayerPrefs.SetString ( "mode", "mouse" );
                break;
            case 5:
                PlayerPrefs.SetFloat ( "target_scale", 0.025f );
                PlayerPrefs.SetFloat ( "target_dist", 20f );
                PlayerPrefs.SetString ( "mode", "mouse" );
                break;
            case 6:
                PlayerPrefs.SetFloat ( "target_scale", 0.075f );
                PlayerPrefs.SetFloat ( "target_dist", 7.5f );
                PlayerPrefs.SetString ( "mode", "controller" );
                break;
            case 7:
                PlayerPrefs.SetFloat ( "target_scale", 0.025f );
                PlayerPrefs.SetFloat ( "target_dist", 7.5f );
                PlayerPrefs.SetString ( "mode", "controller" );
                break;
            case 8: 
                PlayerPrefs.SetFloat ( "target_scale", 0.025f );
                PlayerPrefs.SetFloat ( "target_dist", 20f );
                PlayerPrefs.SetString ( "mode", "controller" );
                break;
            case 9:
                PlayerPrefs.SetFloat ( "target_scale", 0.075f );
                PlayerPrefs.SetFloat ( "target_dist", 7.5f );
                PlayerPrefs.SetString ( "mode", "pointer" );
                break;
            case 10:
                PlayerPrefs.SetFloat ( "target_scale", 0.025f );
                PlayerPrefs.SetFloat ( "target_dist", 7.5f );
                PlayerPrefs.SetString ( "mode", "pointer" );
                break;
            case 12: 
                PlayerPrefs.SetFloat ( "target_scale", 0.025f );
                PlayerPrefs.SetFloat ( "target_dist", 20f );
                PlayerPrefs.SetString ( "mode", "pointer" );
                break;
            }
            setLogPath ( random );
            SceneManager.LoadScene ( "loadScene" );
        } else {
            testDoneMsg.SetActive ( true );
        }
    }

    private bool checkForTestEnd ( ) {
        bool oneSceneUntested = false;
        for ( int i = 0; i < 13; i++ ) {
            if ( PlayerPrefs.GetInt ( "scene" + i ) == 0 ) oneSceneUntested = true;
        }
        return oneSceneUntested;
    }

    public void startFollowTest ( ) {
        PlayerPrefs.SetString ( "mode", "fl_mouse" );
        PlayerPrefs.SetFloat ( "target_scale", 0.05f );
        PlayerPrefs.SetString ( "path", logPathRoot + "fl/" + PlayerPrefs.GetInt ( "userNum" ) + "_mouse" + ".csv" );
        SceneManager.LoadScene ( "loadScene" );

    }

    public void startTestscene ( string scene ) {
        PlayerPrefs.SetString ( "testing", "test" );
        PlayerPrefs.SetString ( "mode", scene );
        setLogPath ( 99 );
        SceneManager.LoadScene ( "loadScene" );
    }
}