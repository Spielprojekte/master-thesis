﻿using UnityEngine;
using System.Collections;

public class AngleT : MonoBehaviour {

    public Transform camera;

    private float distance;
    private float diameter;
    private float angularSize;
    private float pixelSize;
    private Vector3 scrPos;

    public void getDistInPixel ( Transform target, Transform target_d ) {
        diameter = target.GetComponent<Collider> ( ).bounds.extents.magnitude;
        distance = Vector3.Distance ( target.position, camera.position );
        angularSize = ( diameter / distance ) * Mathf.Rad2Deg;
        pixelSize = ( ( angularSize * Screen.height ) / camera.GetComponent<Camera> ( ).fieldOfView );
        scrPos = camera.GetComponent<Camera> ( ).WorldToScreenPoint ( target.position );

        float scrDist = Vector3.Distance ( camera.GetComponent<Camera> ( ).WorldToScreenPoint ( target.position ), camera.GetComponent<Camera> ( ).WorldToScreenPoint ( target_d.position ) );
        Debug.Log ( " D: " + scrDist + " W: " + pixelSize + " Ergebnis: " + ( ( scrDist / pixelSize ) + 1 ) );
    }


}