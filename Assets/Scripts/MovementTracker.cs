﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class MovementTracker : MonoBehaviour {

    public Transform raycastPoint;

    private Vector3 startPos;
    private Vector3 bufferPos;

    private string filepath = "Assets/Resources/Logfiles/mv/";
    private float trackBuffer = 0.0f;
    private float timeTracked = 0.0f;
    private int counter = 0;

    void Start ( ) {
        filepath += ( PlayerPrefs.GetInt ( "userNum" ) + "_" + PlayerPrefs.GetInt ( "scene" ) + ".csv" );
        System.IO.StreamWriter file = new System.IO.StreamWriter ( filepath, true ); 
        file.WriteLine ( "#; Time; Pos_x; Pos_y; Pos_z" );
        file.Close ( );
    }

    void FixedUpdate ( ) {
        trackBuffer += Time.deltaTime;
        timeTracked += Time.deltaTime;
        if ( trackBuffer >= 0.1 ) {
            trackPosition ( );
        }
    }

    private void trackPosition ( ) {
        trackBuffer = 0;
        RaycastHit raycastHit;
        if ( Physics.Raycast ( raycastPoint.position, raycastPoint.forward, out raycastHit ) ) {
            counter++;      

            //writing to file
            System.IO.StreamWriter file = new System.IO.StreamWriter ( PlayerPrefs.GetString ( "path" ), true );   
            //columns are:  "#; Time; Pos_x; Pos_y; Pos_z"
            file.WriteLine ( counter + ";" + timeTracked + ";" + raycastHit.point.x + ";" + raycastHit.point.y + ";" + raycastHit.point.z );
            file.Close ( );
        }
    }
}
