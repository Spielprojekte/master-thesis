﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RedirectScene : MonoBehaviour {

    private float waitTime = 3;
    private float waitedTime = 0;
    private bool loading = false;

    // Use this for initialization
    void Start ( ) {
    }

    private void Update ( ) {
        waitedTime += Time.deltaTime;
        if ( !loading ) {
            if ( waitedTime >= waitTime ) {
                loading = true;
                loadScene ( );         
            }
        }
    }

    public void loadScene ( ) {
        Debug.Log ( "Loading Testscene: " + PlayerPrefs.GetString ( "mode" ) );
        switch ( PlayerPrefs.GetString ( "mode" ) ) {
        case "headmouse":
            SceneManager.LoadSceneAsync ( "sr_vrheadmouse" );
            break;
        case "controller":
            SceneManager.LoadSceneAsync ( "sr_controller" );
            break;
        case "mouse":
            SceneManager.LoadSceneAsync ( "sr_mouse" );
            break;
        case "pointer":
            SceneManager.LoadSceneAsync ( "sr_vrcontroller" );
            break;
        case "fl_mouse":
            SceneManager.LoadSceneAsync ( "fl_mouse" );
            break;
        case "fl_controller":
            SceneManager.LoadSceneAsync ( "fl_controller" );
            break;
        case "fl_vrheadmouse":
            SceneManager.LoadSceneAsync ( "fl_vrheadmouse" );
            break;
        default: 
            SceneManager.LoadSceneAsync ( "start" );
            break;
        }
    }
	
}
