﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetCircle : MonoBehaviour {
    
    public GameObject _targetPrefab;
    public GameObject curTarget;

    [SerializeField]
    private float targetDistance;
    [SerializeField]
    private float targetScale;
    private Vector3 _pivot;
    private int targetCounter = -1;
    private int [] _locations = new int[] {
        0,
        13,
        1,
        14,
        2,
        15,
        3,
        16,
        4,
        17,
        5,
        18,
        6,
        19,
        7,
        20,
        8,
        21,
        9,
        22,
        10,
        23,
        11,
        24,
        12,
        25
    };

    void Start ( ) {
        targetScale = PlayerPrefs.GetFloat ( "target_scale" );
        targetDistance = PlayerPrefs.GetFloat ( "target_dist" );

        if ( targetScale > 0 ) _targetPrefab.transform.localScale = new Vector3 ( targetScale, targetScale, targetScale );
        _pivot = transform.TransformPoint ( transform.position ) / 2;

    }

    public void next ( ) {
        if ( curTarget ) Destroy ( curTarget );
        if ( targetCounter > 25 ) return;
        Vector3 curTargetPos = generateNextLocation ( );
        curTarget = Instantiate ( _targetPrefab, curTargetPos, Quaternion.identity, transform );
        curTarget.transform.Rotate ( new Vector3 ( 90, 0, 0 ) );
        curTarget.name = "target_" + targetCounter;
        targetCounter++;

       
    }

    private Vector3 generateNextLocation ( ) {
        if ( targetCounter == -1 ) {
            return transform.position;
        }
        Vector3 rot = Vector3.up * ( targetDistance / 100 );
        rot = Quaternion.Euler ( new Vector3 ( 0, 0, -15 * _locations [ targetCounter ] ) ) * rot;
        Vector3 iPos = _pivot + ( rot.normalized * ( targetDistance / 10 ) );
        return iPos;
    }

    // test function not to be used in actual program
    private void generateFullCircleOfTargets ( ) {
        Vector3 rot = Vector3.up * ( targetDistance / 100 );
        /*  for ( int j = 0; j < 5; j++ ) {*/
        for ( int i = 0; i < 25; i++ ) {
            rot = Quaternion.Euler ( new Vector3 ( 0, 0, 15 ) ) * rot;
            Vector3 iPos = _pivot + ( rot.normalized * ( targetDistance / 10 ) );
            iPos.z = -15;

            curTarget = Instantiate ( _targetPrefab, iPos, Quaternion.identity, transform );
            curTarget.transform.Rotate ( new Vector3 ( 90, 0, 0 ) );
            curTarget.name = "target_" + i;
        }
    }


}
