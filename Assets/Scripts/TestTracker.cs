﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TestTracker : MonoBehaviour {

    public Transform UIPause;
    public Transform UIMenu;
    public TargetCircle spawnzone;

    private static TestTracker _instance;

    private const float waitTime = 1;
    private const int targetNum = 26;
    private float waitedTime = 0;
    private float startOfRound;
    private float timeBuff;
    private int targetCounter = -1;

    void Start ( ) {
        _instance = this;
        UIMenu.gameObject.SetActive ( false );
    }

    void FixedUpdate ( ) {
        if ( UIPause ) {
            if ( UIPause.gameObject.activeSelf ) {
                waitedTime += Time.deltaTime;
                if ( waitedTime >= waitTime && Input.GetButtonUp ( "Fire" ) ) {
                    startRound ( );
                    waitedTime = 0;
                }
                return;
            }
        }
        if ( UIMenu ) {
            if ( UIMenu.gameObject.activeSelf && Input.GetButtonUp ( "Fire" ) ) {
                SceneManager.LoadScene ( "start" );                           
            }
            return;
        }
    }


    //PUBLIC FUNCTIONS

    public static TestTracker Instance ( ) {
        return _instance;
    }

    /*
     * 
     */
    public void addRecord ( RaycastHit hit ) { 
        
        if ( !UIPause.gameObject.activeSelf && !UIMenu.gameObject.activeSelf ) {
//            Debug.Log ( "Testtracker: Adding Record" );
            //setting values
            bool hitTarget = false;
            float accuracy = 0.0f;
            float speed = Time.time - timeBuff;
            timeBuff = Time.time;
            targetCounter++;

            float distToTargetCenter = Vector3.Distance ( spawnzone.curTarget.transform.position, hit.point );

            if ( hit.transform.parent ) {
                hitTarget = hit.transform.parent.tag == "target" ? true : false;
            }
            if ( hitTarget ) {
                TargetBehaviour tb = hit.transform.parent.GetComponent<TargetBehaviour> ( );
                accuracy = tb ? tb.TakeDamage ( hit.point ) : 0.0f;
            }

            //writing to file
            System.IO.StreamWriter file = new System.IO.StreamWriter ( PlayerPrefs.GetString ( "path" ), true );   
            //columns are: #; Zeitdifferenz; Getroffen; Genauigkeit; Distanz zu ZielmitteZiel_x;Ziel_y;Schuss_x;Schuss_y
            file.WriteLine ( targetCounter + ";" + speed + ";" + hitTarget + ";" + accuracy.ToString ( "F2" ) + ";" + distToTargetCenter + ";" + spawnzone.curTarget.transform.position.x + ";" + spawnzone.curTarget.transform.position.y + ";" + hit.point.x + ";" + hit.point.y );
            file.Close ( );
            if ( targetCounter < targetNum ) {
                spawnzone.next ( );
            } else {
                // end test scenario
                spawnzone.next ( );
                UIMenu.gameObject.SetActive ( true );
            }
        } else {
            closeMenu ( );
        } 

    }

    public bool isMenuOpen ( ) {
        if ( UIMenu.gameObject.activeSelf || UIPause.gameObject.activeSelf ) return true;
        return false;
    }

    public void closeMenu ( ) { 
        Debug.Log ( "calling functioncloseMenu()" );
        if ( UIPause ) {
            if ( UIPause.gameObject.activeSelf ) {
                waitedTime += Time.deltaTime;
                if ( waitedTime >= waitTime ) {
                    startRound ( );
                    waitedTime = 0;
                }
                return;
            }
        }
        if ( UIMenu ) {
            if ( UIMenu.gameObject.activeSelf ) {
                Debug.Log ( "Loading scene start" );
                SceneManager.LoadScene ( "start" );                         
            }
        }        
    }

    //PRIVATE FUNCTIONS

    private void startRound ( ) {
        UIPause.gameObject.SetActive ( false );
        //setting values
        startOfRound = timeBuff = Time.time;
        waitedTime = 0;

        //writing to file
        Debug.Log ( "Writing to file: " + PlayerPrefs.GetString ( "path" ) );
        System.IO.StreamWriter file = new System.IO.StreamWriter ( PlayerPrefs.GetString ( "path" ), false );
        file.WriteLine ( "#;Zeitdifferenz;Getroffen;Genauigkeit;Distanz zu Zielmitte;Ziel_x;Ziel_y;Schuss_x;Schuss_y" );
        file.Close ( );
        //displaying first target
        spawnzone.next ( );
    }

}


