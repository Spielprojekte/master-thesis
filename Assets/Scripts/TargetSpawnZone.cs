﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetSpawnZone : MonoBehaviour {

    public GameObject _targetPrefab;

    private GameObject curTarget;
    private int curTargetNumber = 0;


    public void newTarget ( int roundNumber ) {
        if ( curTarget != null ) Destroy ( curTarget );
        
        Vector3 loc = new Vector3 ( );
        curTargetNumber++;

        // create clone on round-based location
        curTarget = Instantiate ( _targetPrefab, loc, _targetPrefab.transform.rotation );
    }

    public void cleanUp ( ) {
        if ( curTarget != null ) Destroy ( curTarget );               
    }

}
