﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateTextfields : MonoBehaviour {

    public Transform headingText;

    private void Update ( ) {
        if ( headingText ) {
            headingText.Find ( "#" ).GetComponent<Text> ( ).text = ( PlayerPrefs.GetInt ( "userNum" ) ).ToString ( );
            headingText.Find ( "time" ).GetComponent<Text> ( ).text = System.DateTime.Now.ToString ( );
        } else {
            this.enabled = false;
        }
    }
}
