﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetBehaviour : MonoBehaviour {

    [SerializeField]
    private float width = 6;

    public float TakeDamage ( Vector3 hitpoint ) {
        float dist = Vector3.Distance ( transform.position, hitpoint );
        //target center to edge is 1, so to get accuracy with center = 100% one has to say 1-distance
       
        float accuracy = dist / ( transform.localScale.x * width );
        return ( 1 - accuracy );
    }
}
