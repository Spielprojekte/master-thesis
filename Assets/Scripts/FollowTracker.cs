﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnrealFPS;

public class FollowTracker : MonoBehaviour {

    public Transform UIPause;
    public Transform UIMenu;
    public Transform target;
    public Transform raycastPoint;
    public string nextScene = "";
    public string nextLogPath = "";

    private const string logPathRoot = "Assets/Resources/Logfiles/";
    private const float waitTime = 1;
    private bool isTracking = false;
    private float waitedTime = 0;
    private float moveBuffer;
    private float trackBuffer;
    private int counter = 0;

    private Vector3 startPos;
    private Vector3 bufferPos;

    public float duration = 60f;
    public float moveSpeed = 1f;
    public float widthSpan;
    public float heightSpan;

    private void Start ( ) {
        System.IO.StreamWriter file = new System.IO.StreamWriter ( PlayerPrefs.GetString ( "path" ), true ); 
        file.WriteLine ( "#; Getroffen; Genauigkeit; Distanz zu Zielmitte" );
        file.Close ( );

        UIMenu.gameObject.SetActive ( false );

        float targetScale = PlayerPrefs.GetFloat ( "target_scale" );
        if ( targetScale > 0 ) target.localScale = new Vector3 ( targetScale, targetScale, targetScale );
        startPos = bufferPos = target.position;

    }

    private void FixedUpdate ( ) {
        waitedTime += Time.deltaTime;
        if ( UIPause ) {
            if ( UIPause.gameObject.activeSelf ) {
                if ( waitedTime >= waitTime && Input.GetButtonUp ( "Fire" ) ) {
                    waitedTime = 0;
                    UIPause.gameObject.SetActive ( false );
                    isTracking = true;
                }
                return;
            }
        }
        if ( UIMenu ) {
            if ( UIMenu.gameObject.activeSelf && Input.GetButtonUp ( "Fire" ) ) {
                SceneManager.LoadScene ( "loadScene" );
                return;
            }
        } 

        if ( isTracking ) { 
            trackBuffer += Time.deltaTime;
            if ( trackBuffer >= 0.1 ) {
                trackPosition ( );
            }
            moveTarget ( );
            if ( waitedTime >= duration ) {
                isTracking = false;
                UIMenu.gameObject.SetActive ( true );
                PlayerPrefs.SetString ( "mode", nextScene );
                PlayerPrefs.SetString ( "path", logPathRoot + "fl/" + PlayerPrefs.GetInt ( "userNum" ) + "_" + nextLogPath + ".csv" );

            }
        }
    }

    private void moveTarget ( ) {
        if ( Vector3.Distance ( target.position, bufferPos ) < 1 ) {
            // creating new random position in span
            float xPos = Random.Range ( -widthSpan, widthSpan ) + startPos.x;
            float yPos = Random.Range ( -heightSpan, heightSpan ) + startPos.y;
            bufferPos = new Vector3 ( xPos, yPos, target.position.z );

        }
        float step = moveSpeed * Time.deltaTime;
        target.position = Vector3.MoveTowards ( target.position, bufferPos, step );

    }

    private void trackPosition ( ) {
        trackBuffer = 0;
        RaycastHit raycastHit;
        //if ( Physics.Raycast ( attackPoint.position, attackPoint.forward, out raycastHit, attackRange, LayerMask.NameToLayer ( "Targets" ) ) ) {
        if ( Physics.Raycast ( raycastPoint.position, raycastPoint.forward, out raycastHit ) ) {
            counter++;      
            bool hitTarget = false;
            float accuracy = 0.0f;
            float speed = Time.time - moveBuffer;
            moveBuffer = Time.time;

            float distToTargetCenter = Vector3.Distance ( target.position, raycastHit.point );
            if ( raycastHit.transform.parent ) {
                hitTarget = raycastHit.transform.parent.tag == "target" ? true : false;
            }
            if ( hitTarget ) {
                TargetBehaviour tb = raycastHit.transform.parent.GetComponent<TargetBehaviour> ( );
                accuracy = tb ? tb.TakeDamage ( raycastHit.point ) : 0.0f;
            }
            //Debug.Log ( counter + ";" + hitTarget + ";" + accuracy.ToString ( "F2" ) + ";" + distToTargetCenter );

            //writing to file
            System.IO.StreamWriter file = new System.IO.StreamWriter ( PlayerPrefs.GetString ( "path" ), true );   
            //columns are: #; Getroffen; Genauigkeit; Distanz zu Zielmitte
            file.WriteLine ( counter + ";" + hitTarget + ";" + accuracy.ToString ( "F2" ) + ";" + distToTargetCenter );
            file.Close ( );
        }
    }
}
