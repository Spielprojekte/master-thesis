﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class InputTester : MonoBehaviour {

	
    // Update is called once per frame
    void Update ( ) {
        if ( Input.anyKey ) Debug.Log ( "Some key was pressed!" );
        if ( Input.GetKey ( "joystick button 0" ) ) Debug.Log ( "Button 0 was pressed" );
        if ( Input.GetAxis ( "Fire" ) != 0 ) Debug.Log ( "Trigger was pressed" );
        //Debug.Log ( Input.GetAxis ( "Ctrl X" ) + "," + Input.GetAxis ( "Ctrl Y" ) );

    }
}
