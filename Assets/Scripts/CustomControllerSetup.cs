﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class CustomControllerSetup : MonoBehaviour {

    public float XSensitivity = 2;
    public float YSensitivity = 2;
    [Range ( 0.0f, 50.0f )] public float smoothTime = 5f;

    public Transform cam;
    [SerializeField]
    private Quaternion cameraRot;


    // Use this for initialization
    void Start ( ) {
        if ( !cam ) {
            cameraRot = transform.root.localRotation;
        } else {
            cameraRot = cam.localRotation;
            cam.LookAt ( cam );
        }
    }
	
    // Update is called once per frame
    void Update ( ) {
        LookRotation ( );
    }

    public void LookRotation ( ) {
        float yRot = Input.GetAxis ( "Ctrl X" ) * XSensitivity;
        float xRot = Input.GetAxis ( "Ctrl Y" ) * YSensitivity;
        if ( yRot > 0.1 * YSensitivity || yRot < -0.1 * YSensitivity ) yRot = 0;
        if ( xRot > 0.1 * XSensitivity || xRot < -0.1 * XSensitivity ) xRot = 0;

        Debug.Log ( "Rotating camera by: " + xRot + " " + yRot );
        cameraRot *= Quaternion.Euler ( yRot, xRot, 0 );

        transform.root.localRotation = Quaternion.Slerp ( transform.localRotation, cameraRot,
            smoothTime * Time.deltaTime );
        
        
    }
}
