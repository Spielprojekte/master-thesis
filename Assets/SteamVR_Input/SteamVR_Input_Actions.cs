// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 2.0.50727.1433
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

namespace Valve.VR
{
    using System;
    using UnityEngine;
    
    
    public partial class SteamVR_Actions
    {
        
        private static SteamVR_Action_Boolean p_default_Trigger;
        
        private static SteamVR_Action_Skeleton p_default_SkeletonLeftHand;
        
        private static SteamVR_Action_Skeleton p_default_SkeletonRightHand;
        
        private static SteamVR_Action_Boolean p_default_HeadsetOnHead;
        
        private static SteamVR_Action_Pose p_default_pose;
        
        private static SteamVR_Action_Boolean p_default_Menu;
        
        private static SteamVR_Action_Vibration p_default_Haptic;
        
        private static SteamVR_Action_Pose p_mixedreality_ExternalCamera;
        
        private static SteamVR_Action_Vector2 p_gamecontroller_Viewport;
        
        private static SteamVR_Action_Vector2 p_gamecontroller_Movement;
        
        private static SteamVR_Action_Boolean p_gamecontroller_Trigger;
        
        public static SteamVR_Action_Boolean default_Trigger
        {
            get
            {
                return SteamVR_Actions.p_default_Trigger.GetCopy <SteamVR_Action_Boolean>();
            }
        }
        
        public static SteamVR_Action_Skeleton default_SkeletonLeftHand
        {
            get
            {
                return SteamVR_Actions.p_default_SkeletonLeftHand.GetCopy <SteamVR_Action_Skeleton>();
            }
        }
        
        public static SteamVR_Action_Skeleton default_SkeletonRightHand
        {
            get
            {
                return SteamVR_Actions.p_default_SkeletonRightHand.GetCopy <SteamVR_Action_Skeleton>();
            }
        }
        
        public static SteamVR_Action_Boolean default_HeadsetOnHead
        {
            get
            {
                return SteamVR_Actions.p_default_HeadsetOnHead.GetCopy <SteamVR_Action_Boolean>();
            }
        }
        
        public static SteamVR_Action_Pose default_pose
        {
            get
            {
                return SteamVR_Actions.p_default_pose.GetCopy <SteamVR_Action_Pose>();
            }
        }
        
        public static SteamVR_Action_Boolean default_Menu
        {
            get
            {
                return SteamVR_Actions.p_default_Menu.GetCopy <SteamVR_Action_Boolean>();
            }
        }
        
        public static SteamVR_Action_Vibration default_Haptic
        {
            get
            {
                return SteamVR_Actions.p_default_Haptic.GetCopy <SteamVR_Action_Vibration>();
            }
        }
        
        public static SteamVR_Action_Pose mixedreality_ExternalCamera
        {
            get
            {
                return SteamVR_Actions.p_mixedreality_ExternalCamera.GetCopy <SteamVR_Action_Pose>();
            }
        }
        
        public static SteamVR_Action_Vector2 gamecontroller_Viewport
        {
            get
            {
                return SteamVR_Actions.p_gamecontroller_Viewport.GetCopy <SteamVR_Action_Vector2>();
            }
        }
        
        public static SteamVR_Action_Vector2 gamecontroller_Movement
        {
            get
            {
                return SteamVR_Actions.p_gamecontroller_Movement.GetCopy <SteamVR_Action_Vector2>();
            }
        }
        
        public static SteamVR_Action_Boolean gamecontroller_Trigger
        {
            get
            {
                return SteamVR_Actions.p_gamecontroller_Trigger.GetCopy <SteamVR_Action_Boolean>();
            }
        }
        
        private static void InitializeActionArrays()
        {
            Valve.VR.SteamVR_Input.actions = new Valve.VR.SteamVR_Action[]
            {
                    SteamVR_Actions.default_Trigger,
                    SteamVR_Actions.default_SkeletonLeftHand,
                    SteamVR_Actions.default_SkeletonRightHand,
                    SteamVR_Actions.default_HeadsetOnHead,
                    SteamVR_Actions.default_pose,
                    SteamVR_Actions.default_Menu,
                    SteamVR_Actions.default_Haptic,
                    SteamVR_Actions.mixedreality_ExternalCamera,
                    SteamVR_Actions.gamecontroller_Viewport,
                    SteamVR_Actions.gamecontroller_Movement,
                    SteamVR_Actions.gamecontroller_Trigger};
            Valve.VR.SteamVR_Input.actionsIn = new Valve.VR.ISteamVR_Action_In[]
            {
                    SteamVR_Actions.default_Trigger,
                    SteamVR_Actions.default_SkeletonLeftHand,
                    SteamVR_Actions.default_SkeletonRightHand,
                    SteamVR_Actions.default_HeadsetOnHead,
                    SteamVR_Actions.default_pose,
                    SteamVR_Actions.default_Menu,
                    SteamVR_Actions.mixedreality_ExternalCamera,
                    SteamVR_Actions.gamecontroller_Viewport,
                    SteamVR_Actions.gamecontroller_Movement,
                    SteamVR_Actions.gamecontroller_Trigger};
            Valve.VR.SteamVR_Input.actionsOut = new Valve.VR.ISteamVR_Action_Out[]
            {
                    SteamVR_Actions.default_Haptic};
            Valve.VR.SteamVR_Input.actionsVibration = new Valve.VR.SteamVR_Action_Vibration[]
            {
                    SteamVR_Actions.default_Haptic};
            Valve.VR.SteamVR_Input.actionsPose = new Valve.VR.SteamVR_Action_Pose[]
            {
                    SteamVR_Actions.default_pose,
                    SteamVR_Actions.mixedreality_ExternalCamera};
            Valve.VR.SteamVR_Input.actionsBoolean = new Valve.VR.SteamVR_Action_Boolean[]
            {
                    SteamVR_Actions.default_Trigger,
                    SteamVR_Actions.default_HeadsetOnHead,
                    SteamVR_Actions.default_Menu,
                    SteamVR_Actions.gamecontroller_Trigger};
            Valve.VR.SteamVR_Input.actionsSingle = new Valve.VR.SteamVR_Action_Single[0];
            Valve.VR.SteamVR_Input.actionsVector2 = new Valve.VR.SteamVR_Action_Vector2[]
            {
                    SteamVR_Actions.gamecontroller_Viewport,
                    SteamVR_Actions.gamecontroller_Movement};
            Valve.VR.SteamVR_Input.actionsVector3 = new Valve.VR.SteamVR_Action_Vector3[0];
            Valve.VR.SteamVR_Input.actionsSkeleton = new Valve.VR.SteamVR_Action_Skeleton[]
            {
                    SteamVR_Actions.default_SkeletonLeftHand,
                    SteamVR_Actions.default_SkeletonRightHand};
            Valve.VR.SteamVR_Input.actionsNonPoseNonSkeletonIn = new Valve.VR.ISteamVR_Action_In[]
            {
                    SteamVR_Actions.default_Trigger,
                    SteamVR_Actions.default_HeadsetOnHead,
                    SteamVR_Actions.default_Menu,
                    SteamVR_Actions.gamecontroller_Viewport,
                    SteamVR_Actions.gamecontroller_Movement,
                    SteamVR_Actions.gamecontroller_Trigger};
        }
        
        private static void PreInitActions()
        {
            SteamVR_Actions.p_default_Trigger = ((SteamVR_Action_Boolean)(SteamVR_Action.Create <SteamVR_Action_Boolean>("/actions/default/in/Trigger")));
            SteamVR_Actions.p_default_SkeletonLeftHand = ((SteamVR_Action_Skeleton)(SteamVR_Action.Create <SteamVR_Action_Skeleton>("/actions/default/in/SkeletonLeftHand")));
            SteamVR_Actions.p_default_SkeletonRightHand = ((SteamVR_Action_Skeleton)(SteamVR_Action.Create <SteamVR_Action_Skeleton>("/actions/default/in/SkeletonRightHand")));
            SteamVR_Actions.p_default_HeadsetOnHead = ((SteamVR_Action_Boolean)(SteamVR_Action.Create <SteamVR_Action_Boolean>("/actions/default/in/HeadsetOnHead")));
            SteamVR_Actions.p_default_pose = ((SteamVR_Action_Pose)(SteamVR_Action.Create <SteamVR_Action_Pose>("/actions/default/in/pose")));
            SteamVR_Actions.p_default_Menu = ((SteamVR_Action_Boolean)(SteamVR_Action.Create <SteamVR_Action_Boolean>("/actions/default/in/Menu")));
            SteamVR_Actions.p_default_Haptic = ((SteamVR_Action_Vibration)(SteamVR_Action.Create <SteamVR_Action_Vibration>("/actions/default/out/Haptic")));
            SteamVR_Actions.p_mixedreality_ExternalCamera = ((SteamVR_Action_Pose)(SteamVR_Action.Create <SteamVR_Action_Pose>("/actions/mixedreality/in/ExternalCamera")));
            SteamVR_Actions.p_gamecontroller_Viewport = ((SteamVR_Action_Vector2)(SteamVR_Action.Create <SteamVR_Action_Vector2>("/actions/gamecontroller/in/Viewport")));
            SteamVR_Actions.p_gamecontroller_Movement = ((SteamVR_Action_Vector2)(SteamVR_Action.Create <SteamVR_Action_Vector2>("/actions/gamecontroller/in/Movement")));
            SteamVR_Actions.p_gamecontroller_Trigger = ((SteamVR_Action_Boolean)(SteamVR_Action.Create <SteamVR_Action_Boolean>("/actions/gamecontroller/in/Trigger")));
        }
    }
}
