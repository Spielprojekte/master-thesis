﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

using UnityEditorInternal;
using UnityEditor;
using UnityEngine;
using UnrealFPS.UI;

namespace UnrealFPS.Editor
{
    [CustomEditor(typeof(LevelBuilder))]
    public class LevelBuilderEditor : UEditor
    {
        private SerializedProperty e_ButtonTemplate;
        private SerializedProperty e_TemplatesParent;
        private ReorderableList e_LevelParams;

        protected virtual void OnEnable()
        {
            e_ButtonTemplate = serializedObject.FindProperty("levelButtonTemplate");
            e_TemplatesParent = serializedObject.FindProperty("templatesParent");
            e_LevelParams = new ReorderableList(serializedObject, serializedObject.FindProperty("levelParams"), true, true, true, true)
            {
                drawHeaderCallback = (rect) =>
                {
                    EditorGUI.LabelField(new Rect(rect.x + 10, rect.y, 35, EditorGUIUtility.singleLineHeight), "Levels");
                    EditorGUI.LabelField(new Rect(rect.x + 110, rect.y + 1.5f, 360, EditorGUIUtility.singleLineHeight), "Name");
                    EditorGUI.LabelField(new Rect(rect.x + 330, rect.y + 1.5f, 30, EditorGUIUtility.singleLineHeight), "ID");
                },

                drawElementCallback = (rect, index, isActive, isFocused) =>
                {
                    SerializedProperty property = serializedObject.FindProperty("levelParams").GetArrayElementAtIndex(index);
                    EditorGUI.LabelField(new Rect(rect.x + 10, rect.y, 20, EditorGUIUtility.singleLineHeight), (index + 1).ToString());
                    property.FindPropertyRelative("name").stringValue = EditorGUI.TextField(new Rect(rect.x + 50, rect.y + 1.5f, 150, EditorGUIUtility.singleLineHeight), property.FindPropertyRelative("name").stringValue);
                    property.FindPropertyRelative("id").stringValue = EditorGUI.TextField(new Rect(rect.x + 250, rect.y + 1.5f, 150, EditorGUIUtility.singleLineHeight), property.FindPropertyRelative("id").stringValue);
                }
            };
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            BeginBackground();
            Title("Level Builder");
            BeginBox();
            EditorGUILayout.PropertyField(e_ButtonTemplate);
            EditorGUILayout.PropertyField(e_TemplatesParent);
            GUILayout.Space(5);
            e_LevelParams.DoLayoutList();
            EndBox();
            EndBackground();
            serializedObject.ApplyModifiedProperties();
        }
    }
}