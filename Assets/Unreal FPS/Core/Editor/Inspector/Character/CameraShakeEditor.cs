﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

using UnityEditor;

namespace UnrealFPS.Editor
{
    //[CustomEditor(typeof(CameraShake))]
    [CanEditMultipleObjects]
    public class CameraShakeEditor : UEditor
    {
        /// <summary>
        /// Custom Inspector GUI
        /// </summary>
        public override void OnInspectorGUI()
        {
            BeginBackground();
            Title("CameraShake");
            BeginBox();
            EditorGUILayout.HelpBox("Camera Shake Instance", MessageType.Info);
            EndBox();
            EndBackground();
        }
    }
}