﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

using UnityEditor;

namespace UnrealFPS.Editor
{
    [CustomEditor(typeof(FPCameraPostProcessing))]
    [CanEditMultipleObjects]
    public class FPCameraPostProcessingEditor : UEditor
    {
        /// <summary>
        /// Custom Inspector GUI
        /// </summary>
        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            BeginBackground();
            Title("FPCamera Post Processing");
            BeginBox();
            base.OnInspectorGUI();
            EndBox();
            EndBackground();
            serializedObject.ApplyModifiedProperties();
        }
    }
}