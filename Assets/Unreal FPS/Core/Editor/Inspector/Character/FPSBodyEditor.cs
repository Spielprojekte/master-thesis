﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

using UnityEditor;

namespace UnrealFPS.Editor
{
    [CustomEditor(typeof(FPSBody))]
    [CanEditMultipleObjects]
    public class FPSBodyEditor : UEditor
    {
        /// <summary>
        /// Custom inspector GUI.
        /// </summary>
        public override void OnInspectorGUI()
        {
            BeginBackground();
            Title("FPS Body");
            BeginBox();
            base.OnInspectorGUI();
            EndBox();
            EndBackground();
        }
    }
}