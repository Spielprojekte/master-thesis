﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

using UnityEditor;
using UnityEngine;

namespace UnrealFPS.Editor
{
    [CustomEditor(typeof(FPInventory))]
    [CanEditMultipleObjects]
    public class FPInventoryEditor : UEditor
    {
        private SerializedProperty e_FPSCamera;
        private SerializedProperty e_AllowMultipleIdenticalWeapons;
        private SerializedProperty e_DropThrowForce;
        private SerializedProperty e_InventoryGroups;
        private bool isEdit;
        private bool f_Foldout;
        private bool w_Foldout;

        private void OnEnable()
        {
            e_FPSCamera = serializedObject.FindProperty("_FPSCamera");
            e_DropThrowForce = serializedObject.FindProperty("dropThrowForce");
            e_AllowMultipleIdenticalWeapons = serializedObject.FindProperty("allowMultipleIdenticalWeapons");
            e_InventoryGroups = serializedObject.FindProperty("inventoryGroups");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            BeginBackground();
            Title("Inventory");
            GUILayout.BeginVertical("Button");
            GUILayout.Space(5);
            f_Foldout = EditorGUILayout.Foldout(f_Foldout, "Inventory", true);
            if (f_Foldout)
            {
                EditorGUILayout.PropertyField(e_FPSCamera, new GUIContent("FPS Camera"));
                e_AllowMultipleIdenticalWeapons.boolValue = EditorGUILayout.Toggle("Allow Multiple Identical Weapons", e_AllowMultipleIdenticalWeapons.boolValue);
                e_DropThrowForce.floatValue = EditorGUILayout.Slider("Drop Throw Force", e_DropThrowForce.floatValue, 0, 3);

                w_Foldout = EditorGUILayout.Foldout(w_Foldout, "Weapons", true);
                if (w_Foldout)
                {
                    GUILayout.BeginVertical(GUI.skin.box);
                    for (int i = 0; i < e_InventoryGroups.arraySize; i++)
                    {
                        SerializedProperty e_InventoryGroup = e_InventoryGroups.GetArrayElementAtIndex(i);
                        GUILayout.BeginVertical();
                        GUILayout.Space(3);
                        if (!isEdit)
                        {
                            GUILayout.Label(e_InventoryGroup.FindPropertyRelative("name").stringValue, EditorStyles.boldLabel);
                        }
                        else
                        {
                            EditorGUILayout.PropertyField(e_InventoryGroup.FindPropertyRelative("name"), new GUIContent("Group Name"));
                        }

                        GUILayout.BeginVertical(EditorStyles.helpBox);
                        GUILayout.Space(2);
                        for (int j = 0; j < e_InventoryGroup.FindPropertyRelative("inventorySlots").arraySize; j++)
                        {
                            SerializedProperty inventorySlots = e_InventoryGroup.FindPropertyRelative("inventorySlots").GetArrayElementAtIndex(j);
                            GUILayout.BeginHorizontal();
                            GUILayout.Label("Weapon " + (j + 1));
                            GUILayout.Space(1);
                            EditorGUILayout.PropertyField(inventorySlots.FindPropertyRelative("key"), GUIContent.none, GUILayout.Width(100));
                            GUILayout.Space(3);
                            EditorGUILayout.PropertyField(inventorySlots.FindPropertyRelative("weapon"), GUIContent.none);
                            if (GUILayout.Button("x", EditorStyles.miniButton, GUILayout.Width(20), GUILayout.Height(17)))
                            {
                                e_InventoryGroup.FindPropertyRelative("inventorySlots").DeleteArrayElementAtIndex(j);
                            }
                            GUILayout.EndHorizontal();

                        }
                        GUILayout.Space(2);
                        GUILayout.BeginHorizontal();
                        GUILayout.FlexibleSpace();
                        if (GUILayout.Button("Add Weapon", EditorStyles.miniButton, GUILayout.Height(17)))
                        {
                            e_InventoryGroup.FindPropertyRelative("inventorySlots").arraySize++;
                        }
                        if (isEdit)
                        {
                            if (GUILayout.Button("Remove Group", EditorStyles.miniButton, GUILayout.Height(17)))
                            {
                                e_InventoryGroups.DeleteArrayElementAtIndex(i);
                            }
                        }
                        GUILayout.EndHorizontal();
                        GUILayout.Space(2);
                        GUILayout.EndVertical();

                        GUILayout.EndVertical();
                        GUILayout.Space(5);
                    }
                    GUILayout.BeginHorizontal();
                    GUILayout.FlexibleSpace();

                    if (GUILayout.Button("Add Group", EditorStyles.miniButtonLeft, GUILayout.Height(17)))
                    {
                        e_InventoryGroups.arraySize++;
                        e_InventoryGroups.GetArrayElementAtIndex(e_InventoryGroups.arraySize - 1).FindPropertyRelative("name").stringValue = "UnNamed Group";
                        Debug.Log(e_InventoryGroups.arraySize);
                    }
                    if (isEdit && GUILayout.Button("Apply", EditorStyles.miniButtonRight, GUILayout.Height(17)))
                    {
                        isEdit = false;
                    }
                    else if (!isEdit && GUILayout.Button("Edit", EditorStyles.miniButtonRight, GUILayout.Height(17)))
                    {
                        isEdit = true;
                    }
                    GUILayout.EndHorizontal();
                    GUILayout.EndVertical();
                }
            }
            if (!f_Foldout) { GUILayout.Label("Edit Inventory"); }
            GUILayout.Space(5);
            GUILayout.EndVertical();
            EndBackground();
            serializedObject.ApplyModifiedProperties();
        }

    }
}