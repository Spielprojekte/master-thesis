﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

namespace UnrealFPS.Editor
{
    [CustomEditor(typeof(PickUpItem))]
    [CanEditMultipleObjects]
    public class PickUpItemEditor : UEditor
    {
        protected PickUpItem instance;
        private SerializedProperty e_Item;
        private SerializedProperty e_WeaponPrefix;
        private SerializedProperty e_SoundsEffect;
        private SerializedProperty e_AmmoPrefix;
        private SerializedProperty e_BulletCount;
        private SerializedProperty e_ClipCount;
        private SerializedProperty e_IncludingInactiveWeapon;
        private ReorderableList reorderableList;

        protected virtual void OnEnable()
        {
            instance = target as PickUpItem;
            e_Item = serializedObject.FindProperty("item");
            e_WeaponPrefix = serializedObject.FindProperty("weaponPrefix");
            e_AmmoPrefix = serializedObject.FindProperty("ammoPrefix");
            e_BulletCount = serializedObject.FindProperty("bulletCount");
            e_ClipCount = serializedObject.FindProperty("clipCount");
            e_IncludingInactiveWeapon = serializedObject.FindProperty("includingInactiveWeapon");
            e_SoundsEffect = serializedObject.FindProperty("soundEffect");

            reorderableList = new ReorderableList(serializedObject, e_SoundsEffect, true, true, true, true)
            {
                drawHeaderCallback = (rect) => { EditorGUI.LabelField(rect, "Sounds Effect"); },

                drawElementCallback = (rect, index, isActive, isFocused) =>
                {
                    EditorGUI.LabelField(new Rect(rect.x, rect.y + 1.5f, 50, EditorGUIUtility.singleLineHeight), "Clip " + (index + 1));
                    EditorGUI.PropertyField(new Rect(rect.x + 50, rect.y + 1.5f, EditorGUIUtility.currentViewWidth - 135, EditorGUIUtility.singleLineHeight), e_SoundsEffect.GetArrayElementAtIndex(index), GUIContent.none);
                }
            };
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            BeginBackground();
            Title("Pick-Up Item");
            BeginBox();
            GUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.PropertyField(e_Item);
            switch (instance.Item)
            {
                case Item.Weapon:
                    EditorGUILayout.PropertyField(e_WeaponPrefix);
                    break;
                case Item.Ammunition:
                    EditorGUILayout.PropertyField(e_AmmoPrefix);
                    EditorGUILayout.PropertyField(e_BulletCount);
                    EditorGUILayout.PropertyField(e_ClipCount);
                    EditorGUILayout.PropertyField(e_IncludingInactiveWeapon);
                    break;
            }
            GUILayout.EndVertical();
            GUILayout.Space(5);
            reorderableList.DoLayoutList();
            EndBox();
            EndBackground();
            serializedObject.ApplyModifiedProperties();
        }
    }
}