﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace UnrealFPS.Editor
{
    /// <summary>
    /// Custom editor for Weapon Animation System
    /// </summary>
    [CustomEditor(typeof(WeaponAnimationSystem))]
    [CanEditMultipleObjects]
    public class WeaponAnimationSystemEditor : UEditor
    {
        private WeaponAnimationSystem instance;

        protected virtual void OnEnable()
        {
            instance = (WeaponAnimationSystem)target;
        }

        /// <summary>
        /// Custom Inspector GUI
        /// </summary>
        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            BeginBackground();
            Title("Weapon Animation System");
            BeginBox();
            instance.TakeTime = EditorGUILayout.FloatField("Take Time", instance.TakeTime);
            instance.PutAwayTime = EditorGUILayout.FloatField("Put Away Time", instance.PutAwayTime);
            instance.StaticY = EditorGUILayout.FloatField("Set Weapon Y Axis", instance.StaticY);
            instance.MaxYPosJump = EditorGUILayout.FloatField("Max Height", instance.MaxYPosJump);
            instance.SmoothJump = EditorGUILayout.FloatField("Smooth Jump", instance.SmoothJump);
            instance.SmoothLand = EditorGUILayout.FloatField("Smooth Land", instance.SmoothLand);
            instance.UseSway = EditorGUILayout.Toggle("Sway", instance.UseSway);
            GUI.enabled = instance.UseSway;
            instance.Amount = EditorGUILayout.FloatField("Amount", instance.Amount);
            instance.MaxAmount = EditorGUILayout.FloatField("Max Amount", instance.MaxAmount);
            instance.Smooth = EditorGUILayout.FloatField("Smooth Position", instance.Smooth);
            instance.SmoothRotation = EditorGUILayout.FloatField("Smooth Rotation", instance.SmoothRotation);
            instance.TiltAngle = EditorGUILayout.FloatField("Tilt Angle", instance.TiltAngle);
            GUI.enabled = true;
            EndBox();
            EndBackground();
            serializedObject.ApplyModifiedProperties();
        }
    }
}