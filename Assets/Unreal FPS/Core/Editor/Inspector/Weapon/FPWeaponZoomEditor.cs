﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

using UnityEditor;
using UnityEngine;

namespace UnrealFPS.Editor
{
    [CustomEditor(typeof(FPWeaponZoom))]
    [CanEditMultipleObjects]
    public class FPWeaponZoomEditor : UEditor
    {
        protected FPWeaponZoom instance;

        /// <summary>
        /// OnEnable function is called when the object becomes enabled and active.
        /// </summary>
        protected virtual void OnEnable()
        {
            instance = target as FPWeaponZoom;
        }

        /// <summary>
        /// Custom Inspector GUI
        /// </summary>
        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            BeginBackground();
            Title("Weapon Zoom");
            BeginBox();
            base.OnInspectorGUI();
            if(instance.FPCamera == null || instance.WeaponLayer == null)
            {
                EditorGUILayout.HelpBox("FPCamera/WeaponLayer not defined.\nFill manual or press \"Auto Fill\".", MessageType.Warning);
                GUILayout.BeginHorizontal();
                GUILayout.FlexibleSpace();
                if (GUILayout.Button("Auto Fill"))
                    FillCameras(instance.transform.root);
                GUILayout.EndHorizontal();
            }
            EndBox();
            EndBackground();
            serializedObject.ApplyModifiedProperties();
        }

        protected virtual void FillCameras(Transform player)
        {
            Camera[] cameras = player.GetComponentsInChildren<Camera>();
            for (int i = 0; i < cameras.Length; i++)
            {
                if (cameras[i].CompareTag("WeaponCamera"))
                    instance.FPCamera = cameras[i];
                if (cameras[i].CompareTag("WeaponLayer"))
                    instance.WeaponLayer = cameras[i];
            }
        }
    }
}