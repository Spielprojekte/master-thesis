﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

using UnityEditor;
using UnityEngine;
using UnityEditorInternal;
using System.Collections.Generic;
using System;

namespace UnrealFPS.Editor
{
    public struct OptionalComponents
    {
        public string name;
        public string type;
        public bool isActive;
        public Type component;
    }

    public class PlayerBuilder : EditorWindow
    {
        private static Vector2 PlayerBuilderWindowSize = new Vector2(537, 270);
        private GUIStyle titleStyle = new GUIStyle();

        private GameObject player;
        private string playerName = "Player";
        private ReorderableList optionalComponentsList;
        private List<OptionalComponents> optionalComponents = new List<OptionalComponents>();


        public static void Open()
        {
            PlayerBuilder playerBuilderWindow = (PlayerBuilder)GetWindow(typeof(PlayerBuilder), true, "Unreal FPS");
            playerBuilderWindow.minSize = new Vector2(PlayerBuilderWindowSize.x, PlayerBuilderWindowSize.y);
            playerBuilderWindow.maxSize = new Vector2(PlayerBuilderWindowSize.x, PlayerBuilderWindowSize.y);
            playerBuilderWindow.position = new Rect(
                (Screen.currentResolution.width / 2) - (PlayerBuilderWindowSize.x / 2),
                (Screen.currentResolution.height / 2) - (PlayerBuilderWindowSize.y / 2),
                PlayerBuilderWindowSize.x,
                PlayerBuilderWindowSize.y);
            playerBuilderWindow.Show();
        }

        protected virtual void OnEnable()
        {
            player = Resources.Load<GameObject>("Prefab/Player");
            FillComponents(ref optionalComponents);
            optionalComponentsList = new ReorderableList(optionalComponents, typeof(OptionalComponents), false, true, false, false)
            {
                drawHeaderCallback = (rect) => 
                {
                    EditorGUI.LabelField(rect, "Optional Components");
                },

                drawElementCallback = (rect, index, isActive, isFocused) => 
                {
                    OptionalComponents optionalComponent = optionalComponents[index];
                    EditorGUI.LabelField(new Rect(rect.x, rect.y + 1.5f, 200, EditorGUIUtility.singleLineHeight), optionalComponents[index].name);
                    optionalComponent.isActive = EditorGUI.Toggle(new Rect(rect.x + 500, rect.y + 1.5f, 30, EditorGUIUtility.singleLineHeight), optionalComponents[index].isActive);
                    optionalComponents[index] = optionalComponent;
                }
            };
            InitGUIStyles();
        }

        protected virtual void OnGUI()
        {
            GUILayout.Space(10);
            GUILayout.Label("Player Builder", titleStyle);
            GUILayout.Space(10);
            GUILayout.BeginHorizontal();
            GUILayout.Space(5);
            GUILayout.BeginVertical();
            playerName = EditorGUILayout.TextField("Name", playerName);
            GUILayout.Space(5);
            optionalComponentsList.DoLayoutList();
            EditorGUILayout.HelpBox("Add optional components for player.\nYou can add this components later, after create.", MessageType.Info);
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Create"))
                Create();
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUILayout.Space(5);
            GUILayout.EndHorizontal();
        }

        public virtual void Create()
        {
            if(player == null)
            {
                Debug.Log("Player instance not found, in prefab resources folder!");
                return;
            }

            GameObject playerInst = Instantiate(player);
            playerInst.name = playerName;
            for (int i = 0; i < optionalComponents.Count; i++)
            {
                if (optionalComponents[i].isActive && optionalComponents[i].type == "Player")
                    playerInst.AddComponent(optionalComponents[i].component);
                else if (optionalComponents[i].isActive && optionalComponents[i].type == "Camera")
                    playerInst.transform.Find("FPCamera").gameObject.AddComponent(optionalComponents[i].component);
            }
            Debug.Log("Player was created on the scene.");
        }

        public virtual void FillComponents(ref List<OptionalComponents> optionalComponents)
        {
            optionalComponents.Add(new OptionalComponents { name = "Tilts System", type = "Player", isActive = false, component = typeof(FPTilts) });
            optionalComponents.Add(new OptionalComponents { name = "Grab System", type = "Player", isActive = false, component = typeof(FPGrab) });
            optionalComponents.Add(new OptionalComponents { name = "Camera Shake System", type = "Camera", isActive = false, component = typeof(ShakeCamera) });
            optionalComponents.Add(new OptionalComponents { name = "Camera Post Processing System", type = "Camera", isActive = false, component = typeof(FPCameraPostProcessing) });
        }

        protected virtual void InitGUIStyles()
        {
            titleStyle.normal.textColor = Color.grey;
            titleStyle.alignment = TextAnchor.MiddleCenter;
            titleStyle.fontSize = 15;
            titleStyle.fontStyle = FontStyle.Bold;
        }

        public ReorderableList PlayerComponents
        {
            get
            {
                return optionalComponentsList;
            }
        }
    }
}