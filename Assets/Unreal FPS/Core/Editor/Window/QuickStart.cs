﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

using UnityEngine;
using UnityEditor;
using System.Security.Cryptography;
using System;

namespace UnrealFPS.Editor
{
    [InitializeOnLoad]
    public class QuickStart : EditorWindow
    {
        private const string SHOW_ON_START_KEY = "QS_Window_ShowOnStart";

        private static Vector2 QSWindowSize = new Vector2(600, 500);

        public Texture2D mainLogo;
        private Vector2 scrollPos;
        private GUIStyle titleStyle;
        private GUIStyle paragraphStyle;
        private GUIStyle defaultTextStyle;
        private GUIStyle linkStyle;
        private GUIStyle pathsStyle;

        /// <summary>
        /// Static constructor.
        /// </summary>
        static QuickStart()
        {
            EditorApplication.delayCall += () =>
            {
                if (!ShowedOnStart)
                    Open();
            };
        }

        [MenuItem("Help/Unreal FPS Quick Start", false, 0)]
        public static void Open()
        {
            QuickStart qsWindow = (QuickStart)GetWindow(typeof(QuickStart), true, "Unreal FPS");
            qsWindow.minSize = new Vector2(QSWindowSize.x, QSWindowSize.y);
            qsWindow.maxSize = new Vector2(QSWindowSize.x, QSWindowSize.y);
            qsWindow.position = new Rect(
                (Screen.currentResolution.width / 2) - (QSWindowSize.x / 2),
                (Screen.currentResolution.height / 2) - (QSWindowSize.y / 2),
                QSWindowSize.x,
                QSWindowSize.y);
            qsWindow.Show();
        }

        protected virtual void OnEnable()
        {
            if (!mainLogo)
                mainLogo = Resources.Load<Texture2D>("/Icon/Logo.png");
            InitializeGUIStyles();
        }

        /// <summary>
        /// OnGUI is called for rendering and handling GUI events. 
        /// If the MonoBehaviour's enabled property is set to false, OnGUI() will not be called.
        /// </summary>
        protected virtual void OnGUI()
        {
            GUILayout.Space(6);
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label(mainLogo);
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            GUILayout.Space(5);
            GUILayout.Box("", new GUILayoutOption[] { GUILayout.ExpandWidth(true), GUILayout.Height(1) });
            GUILayout.Space(5);

            GUILayout.BeginVertical();
            GUILayout.BeginHorizontal();
            GUILayout.Label("Quick Start", titleStyle);
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();

            GUILayout.Space(5);
            GUILayout.Box("", new GUILayoutOption[] { GUILayout.ExpandWidth(true), GUILayout.Height(1) });
            GUILayout.Space(5);

            scrollPos = GUILayout.BeginScrollView(scrollPos);
            GUILayout.BeginVertical();
            GUILayout.Space(5);
            GUILayout.BeginHorizontal();
            GUILayout.Space(5);
            GUILayout.BeginVertical();

            GUILayout.BeginHorizontal(EditorStyles.helpBox);
            GUILayout.Space(5);
            GUILayout.BeginVertical();
            ParagrapLabel("1: Tags & Layers");
            DefaultLabel("Check your tags for this go to the tab:", defaultTextStyle);
            DefaultLabel("Edit -> Project Settings -> Tags & Layers", pathsStyle);
            DefaultLabel("Create the missing tags:\n   [Weapon]\n   [FPCamera]\n   [FPCameraLayer]\n   [AI]\n   [Ladder]", defaultTextStyle);
            DefaultLabel("", defaultTextStyle);
            DefaultLabel("Create the missing layers:\n   [8 - Player]\n   [9 - Weapon]\n   [10 - AI]\n   [11 - AI Blue Team]" +
                "\n   [12 - AI Red Team]\n   [13 - Remote Body]\n   [14 - Obstacle]", defaultTextStyle);
            GUILayout.Space(5);
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal(EditorStyles.helpBox);
            GUILayout.Space(5);
            GUILayout.BeginVertical();
            ParagrapLabel("2: Input");
            EditorGUILayout.HelpBox("The following steps are only necessary if you are using default Unreal FPS input manager.\nIf you want to use you own/custom input system, skip this step." +
                "\n" +
                "\nUnity can reset input settings after first run scene, we recommended to check the input settings after first playing demo scene and if settings is changed repeat this step.", MessageType.Info);
            DefaultLabel("Setup your input settings for this go to the tab:", defaultTextStyle);
            DefaultLabel("Edit -> Project Settings -> Input", pathsStyle);
            DefaultLabel("Change input names:\n   [Mouse X] -> [mouse_axis_0]\n   [Mouse Y] -> [mouse_axis_1]\n   [MouseScrollWheel] -> [mouse_axis_2]", defaultTextStyle);
            GUILayout.Space(5);
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal(EditorStyles.helpBox);
            GUILayout.Space(5);
            GUILayout.BeginVertical();
            ParagrapLabel("3: Unreal FPS Preferences");
            DefaultLabel("Setup preferences for this go to the tab:", defaultTextStyle);
            DefaultLabel("Edit -> Project Settings -> Preferences", pathsStyle);
            DefaultLabel("Make sure that, paths and color filled correctly.\nIf it completely new project, recommended reset settings.\nFor this press on \"Reset\" buttons.", defaultTextStyle);
            GUILayout.Space(5);
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal(EditorStyles.helpBox);
            GUILayout.Space(5);
            GUILayout.BeginVertical();
            ParagrapLabel("Well done!");

            GUILayout.BeginHorizontal();
            DefaultLabel("For get full informations about \"How to use\" Unreal FPS see - ", defaultTextStyle);
            if (GUILayout.Button("Documentation", linkStyle))
                DocsMenu.OpenAPI();
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            DefaultLabel("To keep abreast of all the new news, follow us on - ", defaultTextStyle);
            if (GUILayout.Button("Twitter", linkStyle))
                Application.OpenURL("https://twitter.com/InfiniteDawnTS");
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            DefaultLabel("If you have any questions you can ask them in the - ", defaultTextStyle);
            if (GUILayout.Button("Official Thread", linkStyle))
                Application.OpenURL("https://forum.unity.com/threads/unreal-fps-official-thread.461248/");
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Сomplete the setup!"))
            {
                this.Close();
            }
            GUILayout.EndHorizontal();

            GUILayout.Space(5);
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();

            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUILayout.EndScrollView();
        }

        protected virtual void OnDestroy()
        {
            EditorPrefs.SetBool(SHOW_ON_START_KEY, true);
        }

        public static bool ShowedOnStart
        {
            get
            {
                return EditorPrefs.GetBool(SHOW_ON_START_KEY);
            }
        }


        public virtual void InitializeGUIStyles()
        {
            titleStyle = new GUIStyle()
            {
                fontSize = 21,
                fontStyle = FontStyle.Bold,
                alignment = TextAnchor.MiddleCenter
            };

            paragraphStyle = new GUIStyle()
            {
                fontSize = 17,
                fontStyle = FontStyle.Bold,
                alignment = TextAnchor.MiddleLeft
            };

            pathsStyle = new GUIStyle()
            {
                fontSize = 12,
                alignment = TextAnchor.MiddleCenter,
                fontStyle = FontStyle.Italic
            };

            defaultTextStyle = new GUIStyle()
            {
                fontSize = 12
            };

            linkStyle = new GUIStyle()
            {
                fontSize = 13,
                fontStyle = FontStyle.Bold
            };
            linkStyle.normal.textColor = Color.blue;
        }

        public virtual void DefaultLabel(string label, GUIStyle style)
        {
            GUILayout.Space(3);
            GUILayout.Label(label, style);
        }

        public virtual void ParagrapLabel(string label)
        {
            GUILayout.Space(5);
            GUILayout.Label(label, paragraphStyle);
            GUILayout.Space(5);
        }
    }
}