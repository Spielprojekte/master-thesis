﻿/* ==================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

/// <summary>
/// Unreal FPS Information
/// </summary>
namespace UnrealFPS
{
    public class Info
    {
        public const string VERSION = "1.13";
        public const string RELEASE = "Unreal FPS Stable";
        public const string AUTHOR = "Tamerlan Favilevich";
        public const string PUBLISHER = "Infinite Dawn";
        public const string COPYRIGHT = "Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.";
    }
}