﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

namespace UnrealFPS
{
    public sealed class UPreferences
    {
        public static float TickPerFrameUI = 0.3f;
    }
}