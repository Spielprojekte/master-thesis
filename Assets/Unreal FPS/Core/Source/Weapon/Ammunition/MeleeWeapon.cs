﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

using UnityEngine;

namespace UnrealFPS
{
    [RequireComponent(typeof(Collider))]
    [RequireComponent(typeof(AudioSource))]
    [RequireComponent(typeof(Rigidbody))]
    public class MeleeWeapon : MonoBehaviour
    {
        [Header("Weapon")]
        [SerializeField] private int damage;
        [SerializeField] private float force;
        [SerializeField] private DecalParams[] weaponHitSurface;

        private AudioSource audioSource;
        
        /// <summary>
        /// Start is called on the frame when a script is enabled just before
        /// any of the Update methods is called the first time.
        /// </summary>
        protected virtual void Start()
        {
            audioSource = GetComponent<AudioSource>();
        }

        /// <summary>
        /// OnCollisionEnter is called when this collider/rigidbody has begun
        /// touching another rigidbody/collider.
        /// </summary>
        /// <param name="other">The Collision data associated with this collision.</param>
        protected virtual void OnCollisionEnter(Collision collision)
        {
            IHealthCallbacks iHealth = collision.transform.GetComponent<IHealthCallbacks>();
            Rigidbody rigidbody = collision.transform.GetComponent<Rigidbody>();

            if (iHealth != null)
                collision.transform.GetComponent<IHealthCallbacks>().TakeDamage(damage);

            if (rigidbody != null)
                rigidbody.AddForce(-collision.transform.forward * force, ForceMode.Impulse);

            Decal.Instantiate(weaponHitSurface, collision, collision.contacts[0], audioSource);
        }

        public int Damage
        {
            get
            {
                return damage;
            }

            set
            {
                damage = value;
            }
        }

        public float Force
        {
            get
            {
                return force;
            }

            set
            {
                force = value;
            }
        }

        public AudioSource AudioSource
        {
            get
            {
                return audioSource;
            }

            set
            {
                audioSource = value;
            }
        }

        public DecalParams[] WeaponHitSurface
        {
            get
            {
                return weaponHitSurface;
            }

            set
            {
                weaponHitSurface = value;
            }
        }
    }
}