﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

using System.Collections;
using UnityEngine;

namespace UnrealFPS
{
    /// <summary>
    /// Grenade behaviour
    /// </summary>
    public class Grenade : MonoBehaviour
    {
        [SerializeField] private float damageRadius;
        [SerializeField] private float shakeRadius;
        [SerializeField] private float power;
        [SerializeField] private int damage;
        [SerializeField] private GameObject effect;
        [SerializeField] private float time;

        /// <summary>
        /// Start is called on the frame when a script is enabled just before
        /// any of the Update methods is called the first time.
        /// </summary>
        protected virtual void Start()
        {
            StartCoroutine(Explosion(time));
        }

        /// <summary>
        /// Explosion grenade after specific time.
        /// </summary>
        /// <param name="time">float: Time in seconds</param>
        /// <returns></returns>
        public virtual IEnumerator Explosion(float time)
        {
            Vector3 explosionPosition = transform.position;
            yield return new WaitForSeconds(time);
            Collider[] colliders = Physics.OverlapSphere(explosionPosition, damageRadius);
            GameObject _e = Instantiate(effect, explosionPosition, Quaternion.identity);
            ParticleSystem ps = _e.GetComponent<ParticleSystem>();
            if (ps != null)
                ps.Play();
            for (int i = 0; i < colliders.Length; i++)
            {
                IHealthCallbacks health = colliders[i].transform.gameObject.GetComponent<IHealthCallbacks>();
                Rigidbody rigidbody = colliders[i].transform.GetComponent<Rigidbody>();

                float distance = Vector3.Distance(explosionPosition, colliders[i].transform.position);
                ShakeCamera.Instance.AddShakeEvent(ShakeCamera.ExplosionShakeProperties(shakeRadius, distance));

                if (health != null)
                    health.TakeDamage(damage);

                if (rigidbody != null)
                    rigidbody.AddExplosionForce(power, explosionPosition, damageRadius, 3.0f);
                yield return null;
            }
            Destroy(gameObject);
            yield break;
        }


        public float Radius { get { return damageRadius; } set { damageRadius = value; } }

        public float Power { get { return power; } set { power = value; } }

        public int Damage { get { return damage; } set { damage = value; } }

        public GameObject Effect { get { return effect; } set { effect = value; } }

        public float Time { get { return time; } set { time = value; } }

        public float ShakeRadius
        {
            get
            {
                return shakeRadius;
            }

            set
            {
                shakeRadius = value;
            }
        }
    }
}