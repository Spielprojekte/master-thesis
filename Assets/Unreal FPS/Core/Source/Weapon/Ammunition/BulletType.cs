﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

namespace UnrealFPS
{
    /// <summary>
    /// Weapon bullet type
    /// </summary>
    public enum BulletType
    {
        Default,
        Fractional
    }
}