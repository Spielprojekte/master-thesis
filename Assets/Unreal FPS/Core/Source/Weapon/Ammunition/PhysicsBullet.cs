﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

using System.Collections;
using UnityEngine;
using UnrealFPS.Utility;

namespace UnrealFPS
{
    [RequireComponent(typeof(Collider))]
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(AudioSource))]
    public class PhysicsBullet : UPoolObject
    {
        [SerializeField] private int damage;
        [SerializeField] private float radius;
        [SerializeField] private float power;
        [SerializeField] private float lifetime;
        [SerializeField] private int numberBullet = 1;
        [SerializeField] private float variance;
        [SerializeField] private bool destroyOnHit;
        [SerializeField] private DecalParams[] bulletHitEffects;

        private AudioSource audioSource;
        
        /// <summary>
        /// Start is called on the frame when a script is enabled just before
        /// any of the Update methods is called the first time.
        /// </summary>
        protected virtual void Start()
        {
            audioSource = GetComponent<AudioSource>();
            StartCoroutine(LifeTime(lifetime));
        }

        /// <summary>
        /// OnCollisionEnter is called when this collider/rigidbody has begun
        /// touching another rigidbody/collider.
        /// </summary>
        /// <param name="other">The Collision data associated with this collision.</param>
        protected virtual void OnCollisionEnter(Collision collision)
        {
            Decal.Instantiate(bulletHitEffects, collision, collision.contacts[0], audioSource);
            OnHitDamage(collision, damage);
            ExplosionForce(power, radius);
            if (destroyOnHit)
                Push();
        }

        /// <summary>
        /// Explosion Handler
        /// </summary>
        /// <param name="explosionPower"></param>
        /// <param name="explosionRadius"></param>
        public virtual void ExplosionForce(float explosionPower, float explosionRadius)
        {
            Vector3 explosionPos = transform.position;
            Collider[] colliders = Physics.OverlapSphere(explosionPos, explosionRadius);
            for (int i = 0; i < colliders.Length; i++)
            {
                IHealthCallbacks health = colliders[i].GetComponent<IHealthCallbacks>();
                if (health != null)
                    health.TakeDamage(damage);

                Rigidbody rb = colliders[i].GetComponent<Rigidbody>();
                if (rb != null)
                    rb.AddExplosionForce(explosionPower, explosionPos, explosionRadius, 3.0F);
            }
        }

        /// <summary>
        /// On Hit Damage Action
        /// </summary>
        /// <param name="collision"></param>
        /// <param name="damageValue"></param>
        public virtual void OnHitDamage(Collision collision, int damageValue)
        {
            if (collision.transform.GetComponent<IHealthCallbacks>() != null)
                collision.gameObject.GetComponent<IHealthCallbacks>().TakeDamage(damage);
        }

        /// <summary>
        /// Destroy buller when timeout
        /// </summary>
        protected virtual IEnumerator LifeTime(float time)
        {
            yield return new WaitForSeconds(time);
            Push();
            yield break;
        }

        public int Damage
        {
            get
            {
                return damage;
            }

            set
            {
                damage = value;
            }
        }

        public float Radius
        {
            get
            {
                return radius;
            }

            set
            {
                radius = value;
            }
        }

        public float Power
        {
            get
            {
                return power;
            }

            set
            {
                power = value;
            }
        }

        public float Lifetime
        {
            get
            {
                return lifetime;
            }

            set
            {
                lifetime = value;
            }
        }

        public int NumberBullet
        {
            get
            {
                return numberBullet;
            }

            set
            {
                numberBullet = value;
            }
        }

        public float Variance
        {
            get
            {
                return variance;
            }

            set
            {
                variance = value;
            }
        }

        public bool DestroyOnHit
        {
            get
            {
                return destroyOnHit;
            }

            set
            {
                destroyOnHit = value;
            }
        }

        public AudioSource AudioSource
        {
            get
            {
                return audioSource;
            }

            set
            {
                audioSource = value;
            }
        }

        public DecalParams[] BulletHitEffects
        {
            get
            {
                return bulletHitEffects;
            }

            set
            {
                bulletHitEffects = value;
            }
        }
    }
}