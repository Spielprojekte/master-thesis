﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

using UnityEngine;

namespace UnrealFPS
{
    public interface IWeaponAnimatorCallbacks
    {
        void PutAway();

        float GetTakeTime();

        float GetPutAwayTime();

        PlayerState[] ActiveState();

        PlayerState[] FullActiveState();
    }
}