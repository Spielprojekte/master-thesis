﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

using UnityEngine;
using UnrealFPS.Utility;
namespace UnrealFPS
{
    [System.Serializable]
    public struct DecalParams
    {
        public string name;
        public PhysicMaterial physicMaterial;
        public Texture2D texture;
        public DecalInstance[] HitDecal;
        public AudioClip[] HitSound;
    }

    public sealed class Decal
    {
        /// <summary>
        /// Create deacl on raycast hit point.
        /// </summary>
        /// <param name="rayCastHit"></param>
        /// <param name="audioSource"></param>
        public static void Instantiate(DecalParams[] decalParam, RaycastHit rayCastHit, AudioSource audioSource)
        {
            Object surfaceInfo = SurfaceHelper.GetSurfaceType(rayCastHit.collider, rayCastHit.point);
            if (!surfaceInfo)
                return;

            for (int i = 0; i < decalParam.Length; i++)
            {
                if (decalParam[i].physicMaterial == surfaceInfo || decalParam[i].texture == surfaceInfo)
                {
                    if (decalParam[i].HitDecal.Length > 0)
                    {
                        int randomDecal = Random.Range(0, decalParam[i].HitDecal.Length);
                        DecalInstance decal = UPoolManager.Instance.PopOrCreate<DecalInstance>(decalParam[i].HitDecal[randomDecal], rayCastHit.point, Quaternion.LookRotation(rayCastHit.normal));
                        decal.transform.SetParent(rayCastHit.collider.transform);
                    }

                    if (decalParam[i].HitSound.Length > 0)
                    {
                        int randomSound = Random.Range(0, decalParam[i].HitSound.Length);
                        audioSource.PlayOneShot(decalParam[i].HitSound[randomSound]);
                    }
                    return;
                }
            }
        }

        /// <summary>
        /// Create decal on collision contact point.
        /// </summary>
        /// <param name="collision"></param>
        /// <param name="contactPoint"></param>
        /// <param name="audioSource"></param>
        public static void Instantiate(DecalParams[] decalParam, Collision collision, ContactPoint contactPoint, AudioSource audioSource)
        {
            Object surfaceInfo = SurfaceHelper.GetSurfaceType(collision.collider, contactPoint.point);
            if (!surfaceInfo)
                return;

            for (int i = 0; i < decalParam.Length; i++)
            {
                if (decalParam[i].physicMaterial == surfaceInfo || decalParam[i].texture == surfaceInfo)
                {
                    if (decalParam[i].HitDecal.Length > 0)
                    {
                        int randomDecal = Random.Range(0, decalParam[i].HitDecal.Length);
                        DecalInstance decal = UPoolManager.Instance.PopOrCreate<DecalInstance>(decalParam[i].HitDecal[randomDecal], contactPoint.point, Quaternion.LookRotation(contactPoint.normal));
                        decal.transform.SetParent(collision.collider.transform);
                    }

                    if (decalParam[i].HitSound.Length > 0)
                    {
                        int randomSound = Random.Range(0, decalParam[i].HitSound.Length);
                        audioSource.PlayOneShot(decalParam[i].HitSound[randomSound]);
                    }
                    return;
                }
            }
        }
    }
}