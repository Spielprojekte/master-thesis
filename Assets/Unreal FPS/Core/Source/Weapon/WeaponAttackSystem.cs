﻿/* =====================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

using UnityEngine;
using UnrealFPS.UI;
using UnrealFPS.Utility;

namespace UnrealFPS {

    public class WeaponAttackSystem : MonoBehaviour {
        [SerializeField] private AttackType attackType;
        [SerializeField] private Transform attackPoint;
        [SerializeField] private PhysicsBullet bullet;
        //For Physics/Throw Attack
        [SerializeField] private RayBullet rayBullet;
        //For RayCast Attack
        [SerializeField] private float delay;
        [SerializeField] private float attackImpulse;
        [SerializeField] private float attackRange;
        [SerializeField] private AudioClip attackSound;
        [SerializeField] private AudioClip emptySound;
        [SerializeField] private FireEffects fireEffects;
        [SerializeField] private SpreadSystem spreadSystem = new SpreadSystem ( );

        private WeaponReloadSystem weaponReloadSystem;
        private IWeaponAnimatorCallbacks weaponAnimatorCallbacks;
        private Crosshair crosshair;
        private AudioSource audioSource;
        private PhysicsBullet physicsBullet;

        private bool isAttack;
        private float s_Delay;

        /// <summary>
        /// Awake is called when the script instance is being loaded.
        /// </summary>
        /// 
        /// <remarks>
        /// Awake is called only once during the lifetime of the script instance.
        /// Awake is always called before any Start functions.
        /// This allows you to order initialization of scripts.
        /// </remarks>
        protected virtual void Awake ( ) {
            audioSource = GetComponent<AudioSource> ( );
            weaponReloadSystem = GetComponent<WeaponReloadSystem> ( );
            weaponAnimatorCallbacks = GetComponent<IWeaponAnimatorCallbacks> ( );
            crosshair = GetComponent<Crosshair> ( );
            if ( attackType == AttackType.Physics ) physicsBullet = bullet.GetComponent<PhysicsBullet> ( );
            spreadSystem.Initialize ( transform.root.GetComponentInChildren<FPController> ( ).Camera.transform, transform.root.GetComponentInChildren<FPController> ( ).MouseLook, attackPoint, weaponAnimatorCallbacks );
            s_Delay = delay;
        }

        /// <summary>
        /// Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        protected virtual void Update ( ) {
            AttackBehaviour ( );
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void AttackBehaviour ( ) {
            if ( UInput.GetButtonDown ( "Fire" ) && !isAttack && !weaponReloadSystem.BulletsIsEmpty && !weaponReloadSystem.IsReloading ) {
                spreadSystem.BulletSpreadProcessing ( );
                if ( attackSound != null ) audioSource.PlayOneShot ( attackSound );
                switch ( attackType ) {
                case AttackType.RayCast:
                    RayCastAttack ( );
                    break;
                case AttackType.Physics:
                    PhysicsAttack ( );
                    break;
                }
                weaponReloadSystem.BulletCount -= 1;
                AttackParticleEffect ( );
                spreadSystem.CameraSpreadProcessing ( );
                isAttack = true;
            } else if ( UInput.GetButtonDown ( "Fire" ) && !isAttack && weaponReloadSystem.BulletsIsEmpty && !weaponReloadSystem.IsReloading ) {
                if ( emptySound != null ) audioSource.PlayOneShot ( emptySound );
                isAttack = true;
            }
            Delay ( );
        }

        public void Shoot ( ) {
            if ( !isAttack && !weaponReloadSystem.BulletsIsEmpty && !weaponReloadSystem.IsReloading ) {
                spreadSystem.BulletSpreadProcessing ( );
                if ( attackSound != null ) audioSource.PlayOneShot ( attackSound );
                switch ( attackType ) {
                case AttackType.RayCast:
                    RayCastAttack ( );
                    break;
                case AttackType.Physics:
                    PhysicsAttack ( );
                    break;
                }
                weaponReloadSystem.BulletCount -= 1;
                AttackParticleEffect ( );
                spreadSystem.CameraSpreadProcessing ( );
                isAttack = true;
                Delay ( );
            }
        }

        protected virtual void RayCastAttack ( ) {
            RaycastHit raycastHit;
            for ( int i = 0; i < rayBullet.Numberbullet; i++ ) {
                if ( rayBullet.Numberbullet > 1 ) attackPoint.localRotation = Quaternion.Euler ( Random.Range ( -rayBullet.Variance, rayBullet.Variance ), Random.Range ( -rayBullet.Variance, rayBullet.Variance ), 0 );
                if ( Physics.Raycast ( attackPoint.position, attackPoint.forward, out raycastHit, attackRange ) ) {
                    
                    Decal.Instantiate ( rayBullet.BulletHitEffects, raycastHit, audioSource );
                    SendDamage ( raycastHit, rayBullet.Damage );
                    if ( raycastHit.rigidbody ) {
                        //Debug.Log ( raycastHit.transform.gameObject.name );
                        raycastHit.rigidbody.AddForceAtPosition ( attackPoint.forward * attackImpulse, raycastHit.point );
                    }
                }
            }
            if ( rayBullet.Numberbullet > 1 ) attackPoint.localRotation = Quaternion.identity;
        }

        protected virtual void PhysicsAttack ( ) {
            for ( int i = 0; i < physicsBullet.NumberBullet; i++ ) {
                if ( physicsBullet.NumberBullet > 1 ) attackPoint.localRotation = Quaternion.Euler ( Random.Range ( -physicsBullet.Variance, physicsBullet.Variance ), Random.Range ( -physicsBullet.Variance, physicsBullet.Variance ), 0 );
                PhysicsBullet bulletClone = UPoolManager.Instance.PopOrCreate<PhysicsBullet> ( bullet, attackPoint.position, attackPoint.rotation );
                bulletClone.GetComponent<Rigidbody> ( ).AddForce ( attackPoint.forward * attackImpulse, ForceMode.Impulse );
            }
            if ( physicsBullet.NumberBullet > 1 ) attackPoint.localRotation = Quaternion.identity;
        }

        /// <summary>
        /// Attack delay.
        /// </summary>
        protected virtual void Delay ( ) {
            if ( isAttack ) {
                delay -= Time.deltaTime;
                if ( delay <= 0 ) {
                    isAttack = false;
                    delay = s_Delay;
                }
            }
        }

        /// <summary>
        /// Send damage from attack.
        /// </summary>
        /// <param name="raycastHit"></param>
        /// <param name="damage"></param>
        protected virtual void SendDamage ( RaycastHit raycastHit, int damage ) {
            if ( raycastHit.transform.root.GetComponent<IHealthCallbacks> ( ) != null ) {
                raycastHit.transform.root.GetComponent<IHealthCallbacks> ( ).TakeDamage ( damage );
                crosshair.HitEffect ( );
            } else {
                //Debug.Log ( raycastHit.transform.name );
                TestTracker.Instance ( ).addRecord ( raycastHit );                
            }
        }

        /// <summary>
        /// Play attack particle effect.
        /// </summary>
        /// 
        /// <remarks>
        /// Played effect when weapon attack.
        /// </remarks>
        protected virtual void AttackParticleEffect ( ) {
            if ( fireEffects.muzzleFlash != null ) fireEffects.muzzleFlash.Play ( );
            if ( fireEffects.cartridgeEjection != null ) fireEffects.cartridgeEjection.Play ( );
        }

        #region [Properties]

        public AttackType W_AttackType { get { return attackType; } set { attackType = value; } }

        public Transform AttackPoint {
            get {
                return attackPoint;
            }

            set {
                attackPoint = value;
            }
        }

        public PhysicsBullet Bullet {
            get {
                return bullet;
            }

            set {
                bullet = value;
            }
        }

        public RayBullet RayBullet {
            get {
                return rayBullet;
            }

            set {
                rayBullet = value;
            }
        }

        public float Delay1 {
            get {
                return delay;
            }

            set {
                delay = value;
            }
        }

        public float AttackImpulse {
            get {
                return attackImpulse;
            }

            set {
                attackImpulse = value;
            }
        }

        public float AttackRange {
            get {
                return attackRange;
            }

            set {
                attackRange = value;
            }
        }

        public AudioClip AttackSound {
            get {
                return attackSound;
            }

            set {
                attackSound = value;
            }
        }

        public AudioClip EmptySound {
            get {
                return emptySound;
            }

            set {
                emptySound = value;
            }
        }

        public SpreadSystem SpreadSystem {
            get {
                return spreadSystem;
            }

            set {
                spreadSystem = value;
            }
        }

        public WeaponReloadSystem WeaponReloadSystem {
            get {
                return weaponReloadSystem;
            }

            set {
                weaponReloadSystem = value;
            }
        }

        public AudioSource AudioSource {
            get {
                return audioSource;
            }

            set {
                audioSource = value;
            }
        }

        public PhysicsBullet PhysicsBullet {
            get {
                return physicsBullet;
            }

            set {
                physicsBullet = value;
            }
        }

        public IWeaponAnimatorCallbacks WeaponAnimatorCallbacks {
            get {
                return weaponAnimatorCallbacks;
            }

            set {
                weaponAnimatorCallbacks = value;
            }
        }

        public FireEffects FireEffects {
            get {
                return fireEffects;
            }

            set {
                fireEffects = value;
            }
        }

        #endregion
    }
}