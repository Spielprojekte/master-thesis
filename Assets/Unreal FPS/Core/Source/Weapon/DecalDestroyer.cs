﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

using System.Collections;
using UnityEngine;

namespace UnrealFPS
{
    [RequireComponent(typeof(DecalInstance))]
    public class DecalDestroyer : MonoBehaviour {

        [SerializeField] private float lifeTime = 5.0f;
        private DecalInstance decalInstance;

        protected virtual void Awake()
        {
            decalInstance = GetComponent<DecalInstance>();
        }

        protected virtual void Start()
        {
            StartCoroutine(Destroyer());
        }

        protected virtual IEnumerator Destroyer()
        {
            yield return new WaitForSeconds(lifeTime);
            decalInstance.Push();
        }
    }
}