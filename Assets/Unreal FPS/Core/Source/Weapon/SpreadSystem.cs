﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace UnrealFPS
{
    [Serializable]
    public struct BulletSpreadParam
    {
        public PlayerState[] state;
        public float maxX;
        public float minX;
        public float maxY;
        public float minY;
    }

    [Serializable]
    public struct CameraSpreadParam
    {
        public PlayerState[] state;
        public float deviationAlongX;
        public bool chakeEffect;
        public float chakeForce;
    }

    [Serializable]
    public class SpreadSystem
    {
        [SerializeField] private BulletSpreadParam[] bulletSpread;
        [SerializeField] private CameraSpreadParam[] cameraSpread;
        

        private Transform camera;
        private NGMouseLook mouseLook;
        private Transform firepoint;
        private IWeaponAnimatorCallbacks weaponAnimatorCallbacks;

        /// <summary>
        /// Initialize is called when the script instance is being loaded.
        /// </summary>
        /// 
        /// <remarks>
        /// Initialize is called only once during the lifetime of the script instance.
        /// Initialize is always called before any Start functions.
        /// </remarks>
        public virtual void Initialize(Transform camera, NGMouseLook mouseLook, Transform firepoint, IWeaponAnimatorCallbacks weaponAnimatorCallbacks)
        {
            this.camera = camera;
            this.mouseLook = mouseLook;
            this.firepoint = firepoint;
            this.weaponAnimatorCallbacks = weaponAnimatorCallbacks;
        }

        /// <summary>
        /// Bullet spread processing, should be called when shoot.
        /// </summary>
        public virtual void BulletSpreadProcessing()
        {
            for (int i = 0; i < bulletSpread.Length; i++)
            {
                if (bulletSpread[i].state.SequenceEqual(weaponAnimatorCallbacks.ActiveState()))
                {
                    float xRot = Random.Range(bulletSpread[i].minX, bulletSpread[i].maxX);
                    float yRot = Random.Range(bulletSpread[i].minY, bulletSpread[i].maxY);
                    firepoint.localRotation = Quaternion.Euler(xRot, yRot, 0);
                    return;
                }
            }
        }

        /// <summary>
        /// Camera spread processing, should be called when shoot.
        /// </summary>
        public virtual void CameraSpreadProcessing()
        {
            for (int i = 0; i < cameraSpread.Length; i++)
            {
                if (cameraSpread[i].state.SequenceEqual(weaponAnimatorCallbacks.ActiveState()))
                {
                    float xRot = (mouseLook.m_CameraTargetRot.x - cameraSpread[i].deviationAlongX);
                    mouseLook.m_CameraTargetRot *= Quaternion.Euler(xRot, 0.0f, 0.0f);
                    if (cameraSpread[i].chakeEffect)
                        camera.localRotation *= Quaternion.Euler(camera.localRotation.x, camera.localRotation.y, cameraSpread[i].chakeForce);
                    return;
                }
            }
            
        }

        public void ResetFirepointRotation()
        {
            firepoint.localRotation = Quaternion.identity;
        }

        public BulletSpreadParam[] BulletSpread
        {
            get
            {
                return bulletSpread;
            }

            set
            {
                bulletSpread = value;
            }
        }

        public CameraSpreadParam[] CameraSpread
        {
            get
            {
                return cameraSpread;
            }

            set
            {
                cameraSpread = value;
            }
        }
    }
}