﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

using UnityEngine;
using UnityEngine.SceneManagement;

namespace UnrealFPS.UI
{
    /// <summary>
    /// Game menu manager.
    /// </summary>
    public class GameMenuManager : IGameMenuCallbacks
    {
        [SerializeField] private bool handleTimeScale = true;
        private RectTransform menuWindow;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// 
        /// <remarks>
        /// Initialize new game menu window.
        /// </remarks>
        /// <param name="menuWindow"></param>
        public GameMenuManager(RectTransform menuWindow)
        {
            this.menuWindow = menuWindow;
        }

        /// <summary>
        /// Display current menu.
        /// </summary>
        /// 
        /// <remarks>
        /// True - HUD enabled / False - HUD disabled.
        /// </remarks>
        /// <param name="display"></param>
        public virtual void DisplayMenu(bool display)
        {
            if (menuWindow == null)
                return;
            menuWindow.gameObject.SetActive(display);
            if (handleTimeScale)
                Time.timeScale = 0;
        }

        /// <summary>
        /// Resume game.
        /// </summary>
        /// 
        /// <remarks>
        /// Hide current menu and resume game.
        /// </remarks>
        public virtual void Resume()
        {
            if (menuWindow == null)
                return;
            menuWindow.gameObject.SetActive(false);
            if (handleTimeScale)
                Time.timeScale = 1;
        }

        /// <summary>
        /// Quit game.
        /// </summary>
        /// 
        /// <remarks>
        /// Quit current game session and load main menu.
        /// </remarks>
        public virtual void Quit()
        {
            SceneManager.LoadScene(SceneManager.GetSceneByBuildIndex(0).name);
        }

        /// <summary>
        /// 
        /// </summary>
        public bool HandleTimeScale
        {
            get
            {
                return handleTimeScale;
            }

            set
            {
                handleTimeScale = value;
            }
        }

        public RectTransform MenuWindow
        {
            get
            {
                return menuWindow;
            }

            set
            {
                menuWindow = value;
            }
        }
    }
}