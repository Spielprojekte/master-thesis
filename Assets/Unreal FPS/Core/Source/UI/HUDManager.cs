﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

using UnityEngine;
using UnityEngine.UI;

namespace UnrealFPS.UI
{
    /// <summary>
    /// Player HUD Components.
    /// </summary>
    [System.Serializable]
    public struct HUDComponents
    {
        public RectTransform hudWindow;
        public Scrollbar healthScrollbar;
        public Text healthPoint;
        public Text bulletCount;
        public Text clipCount;
        public Text weaponName;
        public Image weaponImage;
        public RuntimeMessage runtimeMessage;
    }

    /// <summary>
    /// Runtime message components.
    /// </summary>
    [System.Serializable]
    public struct RuntimeMessage
    {
        public RectTransform messageWindow;
        public Text message;
    }

    /// <summary>
    /// Player UI HUD Manager.
    /// </summary>
    /// 
    /// <remarks>
    /// Processing player HUD components.
    /// </remarks>
    public class HUDManager : IHUDCallbacks
    {
        private HUDComponents components;

        private int lastHealth;
        private int lastBulletCount;
        private int lastClipCount;

        /// <summary>
        /// HUD Manager constructor.
        /// </summary>
        /// 
        /// <remarks>
        /// Initialize new HUD components for the Player.
        /// </remarks>
        /// <param name="_HUDComponents">Player HUD components</param>
        public HUDManager(HUDComponents components)
        {
            this.components = components;
        }

        /// <summary>
        /// Processing health point on the HUD components.
        /// </summary>
        /// <param name="health">Player health point</param>
        public virtual void HealthProcessing(int health, int maxHealth)
        {
            if (lastHealth == health)
                return;

            if (components.healthPoint != null)
                components.healthPoint.text = health.ToString();
            if (components.healthScrollbar != null)
                components.healthScrollbar.size = Mathf.InverseLerp(0, maxHealth, health);
            lastHealth = health;
        }

        /// <summary>
        /// Processing weapon information on the HUD components.
        /// </summary>
        /// <param name="name">Weapon name.</param>
        /// <param name="weaponSprite">Weapon sprite.</param>
        public virtual void WeaponProcessing(string name, Sprite weaponSprite = null)
        {
            if (components.weaponName != null && name != "")
                components.weaponName.text = name;
            else if (name == "")
                components.weaponName.text = "UnNamed Weapon: " + Random.Range(0, 99);

            if (components.weaponImage != null && weaponSprite != null)
                components.weaponImage.sprite = weaponSprite;
        }

        /// <summary>
        /// Processing ammo information on the HUD components.
        /// </summary>
        /// <param name="bulletCount">Weapon bullet count.</param>
        /// <param name="clipCount">Weapon clip count.</param>
        public virtual void AmmoProcessing(int bulletCount, int clipCount)
        {
            if (lastBulletCount != bulletCount && components.bulletCount != null)
            {
                components.bulletCount.text = bulletCount.ToString();
                lastBulletCount = bulletCount;
            }
            if (lastClipCount != clipCount && components.clipCount != null)
            {
                components.clipCount.text = clipCount.ToString();
                lastClipCount = clipCount;
            }
        }

        /// <summary>
        /// Display current HUD.
        /// </summary>
        /// 
        /// <remarks>
        /// True - HUD enabled / False - HUD disabled.
        /// </remarks>
        /// <param name="show"></param>
        public virtual void DisplayHUD(bool show)
        {
            if (components.hudWindow != null)
                components.hudWindow.gameObject.SetActive(show);
        }

        /// <summary>
        /// Display message on the HUD.
        /// </summary>
        /// <param name="message">Message text</param>
        public virtual void DisplayMessage(string message)
        {
            if (components.runtimeMessage.messageWindow == null)
                return;

            components.runtimeMessage.message.text = message;
            components.runtimeMessage.messageWindow.gameObject.SetActive(true);
        }

        /// <summary>
        /// Hide message from the HUD.
        /// </summary>
        public virtual void HideMessage()
        {
            if (components.runtimeMessage.messageWindow == null)
                return;

            components.runtimeMessage.message.text = "";
            components.runtimeMessage.messageWindow.gameObject.SetActive(false);
        }

        /// <summary>
        /// Player HUD components instance.
        /// </summary>
        public HUDComponents GetHUDComponents()
        {
            return components;
        }
    }
}