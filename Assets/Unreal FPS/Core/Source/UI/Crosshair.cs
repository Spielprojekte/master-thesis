﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

using UnityEngine;
using System;
using System.Collections;
using System.Linq;
using UnityEngine.UI;

namespace UnrealFPS.UI
{
    /// <summary>
    /// Crosshair components ("Arrows").
    /// </summary>
    [Serializable]
    public struct CrosshairComponents
    {
        public RectTransform up;
        public RectTransform down;
        public RectTransform left;
        public RectTransform right;
    }

    /// <summary>
    /// Crosshair states
    /// </summary>
    [Serializable]
    public struct CrosshairStates
    {
        public PlayerState[] playerStates;
        public float value;
        public float sec;
    }

    /// <summary>
    /// 
    /// </summary>
    public class Crosshair : MonoBehaviour
    {
        [SerializeField] private CrosshairComponents crosshairComponents;
        [SerializeField] private CrosshairStates[] crosshairStates;
        [SerializeField] private Image[] hitEffect;
        [SerializeField] private float hitEffectSmooth;
        [SerializeField] private bool hideOnZoom;


        private bool crosshairProcessingCoroutineisRunning;
        private bool hitEffectProcessingCoroutineIsRunning;
        private IWeaponAnimatorCallbacks weaponAnimatorCallbacks;
        private PlayerState[] wasPlayerStates;
        private Coroutine startProcessingCoroutine;
        private Coroutine hitEffectCoroutine;


        /// <summary>
        /// Awake is called when the script instance is being loaded.
        /// </summary>
        /// 
        /// <remarks>
        /// Awake is called only once during the lifetime of the script instance.
        /// Awake is always called before any Start functions.
        /// This allows you to order initialization of scripts.
        /// </remarks>
        protected virtual void Awake()
        {
            weaponAnimatorCallbacks = GetComponent<IWeaponAnimatorCallbacks>();
            wasPlayerStates = new PlayerState[1];
        }

        protected virtual void Start()
        {
            if (weaponAnimatorCallbacks == null)
                return;

            StartCoroutine(CrosshairUpdate());
        }

        /// <summary>
        /// Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        protected virtual void Update()
        {
            if (hideOnZoom && UInput.GetButtonDown("Sight"))
                SetVisible(false);
            else if (hideOnZoom && UInput.GetButtonUp("Sight"))
                SetVisible(true);
            ResetHitEffectAlpha(hitEffectSmooth);
        }

        protected virtual IEnumerator CrosshairUpdate()
        {
            while (true)
            {
                yield return new WaitForSeconds(0.1f);
                if (!wasPlayerStates.SequenceEqual(weaponAnimatorCallbacks.ActiveState()))
                {
                    for (int i = 0; i < crosshairStates.Length; i++)
                    {
                        if (crosshairStates[i].playerStates.SequenceEqual(weaponAnimatorCallbacks.ActiveState()))
                        {
                            if (crosshairProcessingCoroutineisRunning)
                                StopCoroutine(startProcessingCoroutine);
                            startProcessingCoroutine = StartCoroutine(StartProcessing(crosshairStates[i].value, crosshairStates[i].sec));
                            break;
                        }
                    }
                    wasPlayerStates = weaponAnimatorCallbacks.ActiveState();
                }
                yield return null;
            }
        }

        public virtual void HitEffect()
        {
            if (hitEffect.Length == 0)
                return;

            if (hitEffectProcessingCoroutineIsRunning)
                StopCoroutine(hitEffectCoroutine);
            hitEffectCoroutine = StartCoroutine(HitEffectProcessing(hitEffectSmooth));
        }

        public virtual void HitEffect(float smooth)
        {
            if (hitEffect.Length == 0)
                return;

            if (hitEffectProcessingCoroutineIsRunning)
            {
                StopCoroutine(hitEffectCoroutine);
                hitEffectProcessingCoroutineIsRunning = false;
            }
            hitEffectCoroutine = StartCoroutine(HitEffectProcessing(smooth));
        }

        protected virtual void ResetHitEffectAlpha(float smooth)
        {
            if (hitEffect.Length == 0)
                return;

            if (!hitEffectProcessingCoroutineIsRunning && !Mathf.Approximately(hitEffect[0].color.a, 0))
            {
                for (int i = 0; i < hitEffect.Length; i++)
                {
                    Color color = hitEffect[i].color;
                    color.a = Mathf.MoveTowards(color.a, 0, Time.deltaTime * smooth);
                    hitEffect[i].color = color;
                }
            }
        }

        /// <summary>
        /// Start animate processing crosshair.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="sec"></param>
        /// <returns></returns>
        public IEnumerator StartProcessing(float value, float sec)
        {
            Vector2 up = new Vector2(0, value);
            Vector2 down = new Vector2(0, -value);
            Vector2 left = new Vector2(-value, 0);
            Vector2 right = new Vector2(value, 0);
            float timeLeft = sec;
            crosshairProcessingCoroutineisRunning = true;
            while (true)
            {
                crosshairComponents.up.localPosition = Vector3.Lerp(crosshairComponents.up.localPosition, up, Time.deltaTime * sec);
                crosshairComponents.down.localPosition = Vector3.Lerp(crosshairComponents.down.localPosition, down, Time.deltaTime * sec);
                crosshairComponents.left.localPosition = Vector3.Lerp(crosshairComponents.left.localPosition, left, Time.deltaTime * sec);
                crosshairComponents.right.localPosition = Vector3.Lerp(crosshairComponents.right.localPosition, right, Time.deltaTime * sec);
                timeLeft -= Time.deltaTime;
                if (timeLeft <= 0)
                {
                    crosshairProcessingCoroutineisRunning = false;
                    yield break;
                }
                yield return null;
            }
        }

        public IEnumerator HitEffectProcessing(float smooth)
        {
            hitEffectProcessingCoroutineIsRunning = true;
            while (true)
            {
                for (int i = 0; i < hitEffect.Length; i++)
                {
                    Color color = hitEffect[i].color;
                    color.a = Mathf.MoveTowards(color.a, 1, Time.deltaTime * smooth);
                    hitEffect[i].color = color;
                }
                if (Mathf.Approximately(hitEffect[hitEffect.Length - 1].color.a, 1))
                {
                    hitEffectProcessingCoroutineIsRunning = false;
                    yield break;
                }

                yield return null;
            }
        }

        public void SetVisible(bool visible)
        {
            crosshairComponents.up.gameObject.SetActive(visible);
            crosshairComponents.down.gameObject.SetActive(visible);
            crosshairComponents.left.gameObject.SetActive(visible);
            crosshairComponents.right.gameObject.SetActive(visible);
        }

        public CrosshairComponents CrosshairComponents
        {
            get
            {
                return crosshairComponents;
            }

            set
            {
                crosshairComponents = value;
            }
        }

        public CrosshairStates[] CrosshairStates
        {
            get
            {
                return crosshairStates;
            }

            set
            {
                crosshairStates = value;
            }
        }

        public bool HideOnZoom
        {
            get
            {
                return hideOnZoom;
            }

            set
            {
                hideOnZoom = value;
            }
        }

        public IWeaponAnimatorCallbacks WeaponAnimatorCallbacks
        {
            get
            {
                return weaponAnimatorCallbacks;
            }

            set
            {
                weaponAnimatorCallbacks = value;
            }
        }
    }
}