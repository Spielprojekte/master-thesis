﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

using UnityEngine;
using UnrealFPS.Utility;

namespace UnrealFPS.UI
{
    /// <summary>
    /// GameUIManager processing UI components.
    /// </summary>
    /// 
    /// <remarks>
    /// GameUIManager is singleton.
    /// Processing all UI components (menu components and HUD components).
    /// And processing/contains GameMenuManager and HUDManager classes.
    /// </remarks>
    public class GameUIManager : Singleton<GameUIManager>
    {
        [SerializeField] RectTransform menuWindow;
        [SerializeField] HUDComponents _HUDComponents;

        private GameMenuManager gameMenuManager;
        private HUDManager hudManager;

        /// <summary>
        /// Awake is called when the script instance is being loaded.
        /// </summary>
        /// 
        /// <remarks>
        /// Awake is used to initialize any variables or game state before the game starts.
        /// Awake is called only once during the lifetime of the script instance.
        /// Awake is always called before any Start functions.
        /// This allows you to order initialization of scripts.
        /// </remarks>
        protected virtual void Awake()
        {
            gameMenuManager = new GameMenuManager(menuWindow);
            hudManager = new HUDManager(_HUDComponents);
        }

        /// <summary>
        /// LateUpdate is called every frame, if the Behaviour is enabled.
        /// </summary>
        /// 
        /// <remarks>
        /// LateUpdate is called after all Update functions have been called. 
        /// This is useful to order script execution. 
        /// For example a follow camera should always be implemented in LateUpdate because it tracks objects that might have moved inside Update.
        /// </remarks>
        protected virtual void LateUpdate()
        {
            if(UInput.GetButtonDown("Menu") && (gameMenuManager.MenuWindow != null && gameMenuManager.MenuWindow.gameObject.activeSelf))
            {
                hudManager.DisplayHUD(false);
                gameMenuManager.DisplayMenu(true);
            }
            else if(UInput.GetButtonDown("Menu") && (gameMenuManager.MenuWindow != null && !gameMenuManager.MenuWindow.gameObject.activeSelf))
            {
                gameMenuManager.DisplayMenu(false);
                hudManager.DisplayHUD(true);
            }
        }
        

        /// <summary>
        /// 
        /// </summary>
        public HUDComponents HUDComponents
        {
            get
            {
                return _HUDComponents;
            }

            set
            {
                _HUDComponents = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public RectTransform MenuWindow
        {
            get
            {
                return menuWindow;
            }

            set
            {
                menuWindow = value;
            }
        }

        public GameMenuManager _GameMenuManager
        {
            get
            {
                return gameMenuManager;
            }

            set
            {
                gameMenuManager = value;
            }
        }

        public HUDManager _HUDManager
        {
            get
            {
                return hudManager;
            }

            set
            {
                hudManager = value;
            }
        }
    }
}