﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

namespace UnrealFPS.UI
{
    /// <summary>
    /// IGameMenuCallbacks interface contains required functions for the GameManuManager.  
    /// </summary>
    public interface IGameMenuCallbacks
    {
        /// <summary>
        /// Display current menu.
        /// </summary>
        /// 
        /// <remarks>
        /// True - HUD enabled / False - HUD disabled.
        /// </remarks>
        /// <param name="display"></param>
        void DisplayMenu(bool display);

        /// <summary>
        /// Resume game.
        /// </summary>
        /// 
        /// <remarks>
        /// Hide current menu and resume game.
        /// </remarks>
        void Resume();

        /// <summary>
        /// Quit game.
        /// </summary>
        /// 
        /// <remarks>
        /// Quit current game session and load main menu.
        /// </remarks>
        void Quit();
    }
}
