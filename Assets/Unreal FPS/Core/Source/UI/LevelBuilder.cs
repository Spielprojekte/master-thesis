﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace UnrealFPS.UI
{
    [System.Serializable]
    public struct LevelParam
    {
        public string name;
        public string id;
    }

    public class LevelBuilder : MonoBehaviour
    {
        [SerializeField] private LevelParam[] levelParams;
        [SerializeField] private RectTransform levelButtonTemplate;
        [SerializeField] private RectTransform templatesParent;


        /// <summary>
        /// Awake is called when the script instance is being loaded.
        /// </summary>
        /// 
        /// <remarks>
        /// Awake is called only once during the lifetime of the script instance.
        /// Awake is always called before any Start functions.
        /// This allows you to order initialization of scripts.
        /// </remarks>
        protected virtual void Awake()
        {
            BuildLeves(levelParams, levelButtonTemplate, templatesParent);
        }

        public virtual void BuildLeves(LevelParam[] levelParams, RectTransform levelButtonTemplate, RectTransform templatesParent)
        {
            for (int i = 0; i < levelParams.Length; i++)
            {
                GameObject level = Instantiate(levelButtonTemplate.gameObject);
                level.name = LevelParams[i].name;
                level.transform.SetParent(templatesParent);
                string levelID = LevelParams[i].id;
                level.transform.GetComponent<Button>().onClick.AddListener(() => { LoadLevel(levelID); });
                level.transform.GetChild(0).GetComponent<Text>().text = levelParams[i].name;
            }
        }

        public void LoadLevel(string id)
        {
            int sceneID;
            bool isNumber = int.TryParse(id, out sceneID);

            if (isNumber)
                SceneManager.LoadScene(sceneID);
            else
                SceneManager.LoadScene(id);
        }

        public LevelParam[] LevelParams
        {
            get
            {
                return levelParams;
            }

            set
            {
                levelParams = value;
            }
        }

        public RectTransform LevelButtonTemplate
        {
            get
            {
                return levelButtonTemplate;
            }

            set
            {
                levelButtonTemplate = value;
            }
        }

        public RectTransform TemplatesParent
        {
            get
            {
                return templatesParent;
            }

            set
            {
                templatesParent = value;
            }
        }
    }
}