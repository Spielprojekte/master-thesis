﻿using UnityEngine;
using System.Collections;

namespace UnrealFPS
{
	public enum ScanFlags
	{
		None = 0, Key = 2, JoystickButton = 4, JoystickAxis = 8, MouseAxis = 16
	}
}