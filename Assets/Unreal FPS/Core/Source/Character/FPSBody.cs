﻿/* =====================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

using UnityEngine;

namespace UnrealFPS {
    [RequireComponent ( typeof ( Animator ) )]
    public class FPSBody : MonoBehaviour {
        [SerializeField] private float stayPoseHeight = -1.55f;
        [SerializeField] private float crouchPoseHeight = 0.55f;
        [SerializeField] private InverseKinematics inverseKinematics = new InverseKinematics ( );

        private FPController controller;
        private Animator animator;

        /// <summary>
        /// Start is called on the frame when a script is enabled just before
        /// any of the Update methods is called the first time.
        /// </summary>
        private void Start ( ) {
            animator = GetComponent<Animator> ( );
            controller = transform.root.GetComponentInChildren<FPController> ( );
            inverseKinematics.Init ( animator );
        }

        /// <summary>
        /// Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        private void Update ( ) {
            float speed = UInput.GetAxis ( "Vertical" );
            float direction = UInput.GetAxis ( "Horizontal" );
            if ( controller.IsRunning ) speed = 2;
            animator.SetFloat ( "Speed", speed );
            animator.SetFloat ( "Direction", direction );
            animator.SetBool ( "IsCrouching", controller.FPCrouch.IsCrouch );
            float fixedVerticalPosition;
            if ( controller.FPCrouch.IsCrouch ) fixedVerticalPosition = Mathf.MoveTowards ( transform.localPosition.y, crouchPoseHeight, 7 * Time.deltaTime );
            else fixedVerticalPosition = Mathf.MoveTowards ( transform.localPosition.y, stayPoseHeight, 7 * Time.deltaTime );
            transform.localPosition = new Vector3 ( transform.localPosition.x, fixedVerticalPosition, transform.localPosition.z );
        }

        /// <summary>
        /// Callback for setting up animation IK (inverse kinematics).
        /// </summary>
        /// <param name="layerIndex">Index of the layer on which the IK solver is called.</param>
        void OnAnimatorIK ( int layerIndex ) {
            inverseKinematics.FootIK ( );
        }

        public float StayPoseHeight {
            get {
                return stayPoseHeight;
            }

            set {
                stayPoseHeight = value;
            }
        }

        public float CrouchPoseHeight {
            get {
                return crouchPoseHeight;
            }

            set {
                crouchPoseHeight = value;
            }
        }

        public InverseKinematics _InverseKinematics {
            get {
                return inverseKinematics;
            }

            set {
                inverseKinematics = value;
            }
        }

        public FPController Controller {
            get {
                return controller;
            }

            set {
                controller = value;
            }
        }

        public Animator _Animator {
            get {
                return animator;
            }

            set {
                animator = value;
            }
        }
    }
}