﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnrealFPS.UI;

namespace UnrealFPS
{
    /// <summary>
    /// Fal Damage struct
    /// </summary>
    [System.Serializable]
    public struct FallDamage
    {
        public float minHeight;
        public float maxHeight;
        public int damage;
    }

    [System.Serializable]
    public struct RegenirationSettings
    {
        public float interval;
        public int value;
        public float time;
    }

    /// <summary>
    /// Base Player Health class
    /// </summary>
    public class PlayerHealth : MonoBehaviour, IHealthCallbacks
    {
        [SerializeField] private int health;
        [SerializeField] private int maxHealth;
        [SerializeField] private int startHealth;
        [SerializeField] private List<FallDamage> fallDamages = new List<FallDamage>();
        [SerializeField] private UnityEvent onDeadEvent;
        [SerializeField] private KillCam killCam;
        [SerializeField] private bool useRegeniration;
        [SerializeField] private RegenirationSettings regenerationSettings;

        private FPController controller;
        private CharacterController characterController;
        private Coroutine regenerationCoroutine;
        private bool regenerationCoroutineIsRunning;
        private float lastHeightPosition;
        private bool isStarting;


        /// <summary>
        /// Awake is called when the script instance is being loaded.
        /// </summary>
        /// 
        /// <remarks>
        /// Awake is called only once during the lifetime of the script instance.
        /// Awake is always called before any Start functions.
        /// This allows you to order initialization of scripts.
        /// </remarks>
        protected virtual void Awake()
        {
            controller = GetComponent<FPController>();
            characterController = GetComponent<CharacterController>();
            health = startHealth;
        }

        /// <summary>
        /// Start is called on the frame when a script is enabled just before any of the Update methods are called the first time.
        /// </summary>
        /// 
        /// <remarks>
        /// Like the Awake function, Start is called exactly once in the lifetime of the script. 
        /// However, Awake is called when the script object is initialised, regardless of whether or not the script is enabled. 
        /// Start may not be called on the same frame as Awake if the script is not enabled at initialisation time.
        /// </remarks>
        protected virtual void Start()
        {
            killCam.Init(controller.Camera.transform, characterController);
        }

        /// <summary>
        /// Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        protected virtual void Update()
        {
            HealthHandler();
            GameUIManager.Instance._HUDManager.HealthProcessing(health, maxHealth);
            if (IsAlive)
                FallDamage(characterController.isGrounded);
        }

        /// <summary>
        /// Player Health handler
        /// </summary>
        public virtual void HealthHandler()
        {
            if (!IsAlive)
            {
                OnDead();
                controller.LockMovement = true;
                killCam.Play();
                isStarting = true;
            }
            else if (isStarting)
            {
                killCam.Reset();
                controller.LockMovement = false;
                isStarting = false;
            }
        }

        /// <summary>
        /// Health Regenegation System
        /// </summary>
        public void HealthRegeneration()
        {
            if (!useRegeniration)
                return;

            if (regenerationCoroutineIsRunning)
                StopCoroutine(regenerationCoroutine);
            regenerationCoroutine = StartCoroutine(Regenerate(regenerationSettings));
        }

        public IEnumerator Regenerate(RegenirationSettings regenerationSettings)
        {
            bool waitBeforeStart = true;
            bool playRegenerate = false;
            regenerationCoroutineIsRunning = true;
            while (true)
            {
                while (waitBeforeStart)
                {
                    float lastHealth = health;
                    yield return new WaitForSeconds(regenerationSettings.time);
                    if (lastHealth == health)
                    {
                        waitBeforeStart = false;
                        playRegenerate = true;
                        break;
                    }
                }

                while (playRegenerate)
                {
                    health += regenerationSettings.value;
                    float lastHealth = health;
                    yield return new WaitForSeconds(regenerationSettings.interval);
                    if (health >= maxHealth)
                    {
                        health = maxHealth;
                        regenerationCoroutineIsRunning = false;
                        yield break;
                    }
                    else if (lastHealth != health)
                    {
                        waitBeforeStart = true;
                        playRegenerate = false;
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Take damage
        /// </summary>
        /// <param name="amount"></param>
        public void TakeDamage(int amount)
        {
            health -= amount;
            ShakeCamera.Instance.AddShakeEvent(ShakeCamera.HitShakeProperties());
            HealthRegeneration();
        }

        public virtual void OnDead()
        {
            onDeadEvent.Invoke();
        }

        public virtual void FallDamage(bool isGrounded)
        {
            if (!isGrounded)
            {
                if (lastHeightPosition < transform.position.y)
                {
                    lastHeightPosition = transform.position.y;
                }
            }
            else if (lastHeightPosition > transform.position.y)
            {
                float distance = lastHeightPosition - transform.position.y;
                for (int i = 0; i < fallDamages.Count; i++)
                {
                    if (distance > fallDamages[i].minHeight && distance < fallDamages[i].maxHeight)
                    {
                        TakeDamage(fallDamages[i].damage);
                        lastHeightPosition = transform.position.y;
                    }
                }
            }
        }

        /// <summary>
        /// Player health
        /// </summary>
        public int Health
        {
            get
            {
                return health;
            }

            set
            {
                health = value;
            }
        }

        /// <summary>
        /// Player is alive
        /// </summary>
        public bool IsAlive
        {
            get
            {
                return health > 0;
            }
        }

        public int MaxHealth { get { return maxHealth; } set { maxHealth = value; } }

        public float HealthPercent { get { return ((float)health / maxHealth) * 100; } }

        public int StartHealth
        {
            get
            {
                return startHealth;
            }

            set
            {
                startHealth = value;
            }
        }

        public List<FallDamage> FallDamages
        {
            get
            {
                return fallDamages;
            }

            set
            {
                fallDamages = value;
            }
        }

        public UnityEvent OnDeadEvent
        {
            get
            {
                return onDeadEvent;
            }

            set
            {
                onDeadEvent = value;
            }
        }

        public KillCam KillCam
        {
            get
            {
                return killCam;
            }

            set
            {
                killCam = value;
            }
        }

        public bool UseRegeniration
        {
            get
            {
                return useRegeniration;
            }

            set
            {
                useRegeniration = value;
            }
        }

        public RegenirationSettings RegenerationSettings
        {
            get
            {
                return regenerationSettings;
            }

            set
            {
                regenerationSettings = value;
            }
        }

        public FPController Controller
        {
            get
            {
                return controller;
            }

            set
            {
                controller = value;
            }
        }

        public CharacterController CharacterController
        {
            get
            {
                return characterController;
            }

            set
            {
                characterController = value;
            }
        }
    }
}