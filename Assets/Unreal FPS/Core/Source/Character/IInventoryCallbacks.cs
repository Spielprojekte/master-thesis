﻿/* ==================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

using UnityEngine;

namespace UnrealFPS
{
    /// <summary>
    /// IInventoryCallbacks interface contains require inventory callbacks.
    /// </summary>
    public interface IInventoryCallbacks
	{
		/// <summary>
		/// Add weapon.
		/// </summary>
		/// <param name="weapon"></param>
		void AddWeapon(Weapon weapon);

        /// <summary>
        /// Drop current active weapon.
        /// </summary>
        void DropWeapon();

        /// <summary>
        /// Drop weapon.
        /// </summary>
        /// <param name="weapon"></param>
        void DropWeapon(string id);

		/// <summary>
		/// Activate weapon by id.
		/// </summary>
		/// <param name="id"></param>
		void ActivateWeapon(string id);

        /// <summary>
        /// Activate next weapon in inventory.
        /// If active weapon last in inventory, will activate first weapon from inventory.
        /// </summary>
        void ActivateNextWeapon();

        /// <summary>
        /// Activate previous weapon in inventory.
        /// If active weapon is first in inventory, will activate last weapon from inventory.
        /// </summary>
        void ActivatePreviousWeapon();

        /// <summary>
        /// Activate deactivated weapon.
        /// </summary>
        void ActivateLastWeapon();

        /// <summary>
        /// Deactivate current active weapon.
        /// </summary>
        void DeactivateWeapon();

        /// <summary>
        /// Deativate weapon by id.
        /// </summary>
        /// <param name="id"></param>
        void DeactivateWeapon(string id);

        /// <summary>
		/// Get current active weapon.
		/// </summary>
		/// <returns>Transform: Active weapon</returns>
		Transform GetActiveWeapon();

        /// <summary>
        /// Get weapon by index from FPCamera childs.
        /// </summary>
        /// <param name="index">Index from FPCamera childs</param>
        /// <returns>Transform: By specific index</returns>
        Transform GetWeapon(int index);

        /// <summary>
        /// Get weapon by id from FPCamera childs.
        /// </summary>
        /// <param name="id">ID form WeaponIdentifier component</param>
        /// <returns>Transform: By specific id</returns>
        Transform GetWeapon(string id);

        /// <summary>
        /// Return next weapon in inventory.
        /// If active weapon last in inventory, will return first weapon from inventory.
        /// </summary>
        /// <returns>Transform: next weapon</returns>
        Transform GetNextWeapon();

        /// <summary>
        /// Return previous weapon in inventory.
        /// If active weapon is first in inventory, will return last weapon from inventory.
        /// </summary>
        /// <returns>Transform: previous weapon</returns>
        Transform GetPreviousWeapon();
    }
}