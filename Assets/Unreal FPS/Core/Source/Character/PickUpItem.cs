﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

using UnityEngine;
using UnrealFPS.UI;
namespace UnrealFPS
{
    public enum Item { Weapon, Ammunition }

    [RequireComponent(typeof(WeaponIdentifier))]
    [RequireComponent(typeof(Collider))]
    [RequireComponent(typeof(AudioSource))]
    public class PickUpItem : MonoBehaviour
    {
        [SerializeField] private Item item;
        [SerializeField] private string weaponPrefix;
        [SerializeField] private bool destroyOnPickUp;
        [SerializeField] private AudioClip[] soundEffect;
        [SerializeField] private string ammoPrefix;
        [SerializeField] private int bulletCount;
        [SerializeField] private int clipCount;
        [SerializeField] private bool includingInactiveWeapon;


        private WeaponIdentifier weaponIdentifier;
        private Collider _collider;
        private AudioSource audioSource;

        protected virtual void Awake()
        {
            weaponIdentifier = GetComponent<WeaponIdentifier>();
            _collider = GetComponent<Collider>();
            audioSource = GetComponent<AudioSource>();
        }


        protected virtual void OnTriggerStay(Collider collider)
        {
            IInventoryCallbacks inventoryCallbacks = collider.GetComponent<IInventoryCallbacks>();
            if (inventoryCallbacks == null)
                return;

            if (item == Item.Weapon)
            {
                if (UInput.GetButtonDown("PickUp"))
                {
                    inventoryCallbacks.AddWeapon(weaponIdentifier.Weapon);
                    PlaySoundEffect(soundEffect);
                    if (destroyOnPickUp)
                        Destroy(gameObject);
                }
                GameUIManager.Instance._HUDManager.DisplayMessage(weaponPrefix + weaponIdentifier.Weapon.DisplayName);
            }
            else if (item == Item.Ammunition)
            {
                Transform playerWeaponTransform = inventoryCallbacks.GetActiveWeapon();
                if (playerWeaponTransform != null)
                {
                    Weapon playerWeapon = playerWeaponTransform.GetComponent<WeaponIdentifier>().Weapon;
                    if (playerWeapon != null && weaponIdentifier.Weapon.Id == playerWeapon.Id)
                    {
                        WeaponReloadSystem weaponReloadSystem = playerWeaponTransform.GetComponent<WeaponReloadSystem>();
                        WeaponAttackSystem weaponAttackSystem = playerWeaponTransform.GetComponent<WeaponAttackSystem>();
                        if (weaponReloadSystem != null)
                        {
                            if (UInput.GetButtonDown("PickUp"))
                            {
                                weaponReloadSystem.BulletCount += bulletCount;
                                weaponReloadSystem.ClipCount += clipCount;
                                weaponReloadSystem.ReCalculateAmmo();
                                PlaySoundEffect(soundEffect);
                                if (destroyOnPickUp)
                                    Destroy(gameObject);
                            }
                            string ammoName = "Ammunition";
                            if (weaponAttackSystem != null)
                            {
                                switch (weaponAttackSystem.W_AttackType)
                                {
                                    case AttackType.RayCast:
                                        ammoName = weaponAttackSystem.RayBullet.Model;
                                        break;
                                    case AttackType.Physics:
                                        if (weaponAttackSystem.Bullet != null)
                                            ammoName = weaponAttackSystem.Bullet.name;
                                        break;
                                }
                            }
                            GameUIManager.Instance._HUDManager.DisplayMessage(weaponPrefix + ammoName);
                        }
                        return;
                    }
                }
                if (includingInactiveWeapon)
                {
                    Transform weaponTransform = inventoryCallbacks.GetWeapon(weaponIdentifier.Weapon.Id);
                    if (weaponTransform != null)
                    {
                        WeaponReloadSystem weaponReloadSystem = weaponTransform.GetComponent<WeaponReloadSystem>();
                        WeaponAttackSystem weaponAttackSystem = playerWeaponTransform.GetComponent<WeaponAttackSystem>();

                        if (weaponReloadSystem != null)
                        {
                            if (UInput.GetButtonDown("PickUp"))
                            {
                                weaponReloadSystem.BulletCount += bulletCount;
                                weaponReloadSystem.ClipCount += clipCount;
                                weaponReloadSystem.ReCalculateAmmo();
                                PlaySoundEffect(soundEffect);
                                if (destroyOnPickUp)
                                    Destroy(gameObject);
                            }
                            string ammoName = "Ammunition";
                            if (weaponAttackSystem != null)
                            {
                                switch (weaponAttackSystem.W_AttackType)
                                {
                                    case AttackType.RayCast:
                                        ammoName = weaponAttackSystem.RayBullet.Model;
                                        break;
                                    case AttackType.Physics:
                                        if (weaponAttackSystem.Bullet != null)
                                            ammoName = weaponAttackSystem.Bullet.name;
                                        break;
                                }
                            }
                            GameUIManager.Instance._HUDManager.DisplayMessage(weaponPrefix + ammoName);
                        }
                    }
                }
            }
        }

        protected virtual void OnTriggerExit(Collider collider)
        {
            GameUIManager.Instance._HUDManager.HideMessage();
        }

        public virtual void PlaySoundEffect(AudioClip[] clips)
        {
            int length = clips.Length;
            int index = Random.Range(0, length - 1);
            if (length > 0)
                audioSource.PlayOneShot(clips[index]);
        }

        public WeaponIdentifier _WeaponIdentifier
        {
            get
            {
                return weaponIdentifier;
            }

            set
            {
                weaponIdentifier = value;
            }
        }

        public Collider _Collider
        {
            get
            {
                return _collider;
            }

            set
            {
                _collider = value;
            }
        }

        public AudioSource _AudioSource
        {
            get
            {
                return audioSource;
            }

            set
            {
                audioSource = value;
            }
        }

        public Item Item
        {
            get
            {
                return item;
            }

            set
            {
                item = value;
            }
        }

        public AudioClip[] SoundEffect
        {
            get
            {
                return soundEffect;
            }

            set
            {
                soundEffect = value;
            }
        }

        public int BulletCount
        {
            get
            {
                return bulletCount;
            }

            set
            {
                bulletCount = value;
            }
        }

        public int ClipCount
        {
            get
            {
                return clipCount;
            }

            set
            {
                clipCount = value;
            }
        }

        public bool IncludingInactiveWeapon
        {
            get
            {
                return includingInactiveWeapon;
            }

            set
            {
                includingInactiveWeapon = value;
            }
        }
    }
}