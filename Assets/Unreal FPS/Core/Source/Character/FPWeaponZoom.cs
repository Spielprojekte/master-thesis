﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

using UnityEngine;
using System.Collections;

namespace UnrealFPS
{
    public class FPWeaponZoom : MonoBehaviour
    {
        [SerializeField] private Camera _FPCamera;
        [SerializeField] private Camera _WeaponLayer;
        [SerializeField] private float fovValue;
        [SerializeField] private float smooth;

        private float wasFOVValue;
        private bool zoomCoroutineIsRunning;


        /// <summary>
        /// Start is called on the frame when a script is enabled just before any of the Update methods are called the first time.
        /// </summary>
        /// 
        /// <remarks>
        /// Like the Awake function, Start is called exactly once in the lifetime of the script. 
        /// However, Awake is called when the script object is initialised, regardless of whether or not the script is enabled. 
        /// Start may not be called on the same frame as Awake if the script is not enabled at initialisation time.
        /// </remarks>
        protected virtual void Start()
        {
            wasFOVValue = _FPCamera.fieldOfView;
        }

        /// <summary>
        /// Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        protected virtual void Update()
        {
            if (UInput.GetButtonDown("Sight"))
                ZoomProcessing(1);
            else if (UInput.GetButtonUp("Sight"))
                ZoomProcessing(0);
        }

        /// <summary>
        /// Zoom processing.
        ///     Codes:
        ///         1: Zoom on.
        ///         0: Zoom out.
        /// </summary>
        protected virtual void ZoomProcessing(byte code)
        {
            if (zoomCoroutineIsRunning)
                StopAllCoroutines();
            StartCoroutine(ZoomCoroutine(code));
        }

        /// <summary>
        /// Zoom processing coroutine.
        ///     Codes:
        ///         1: Zoom on.
        ///         0: Zoom out.
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public virtual IEnumerator ZoomCoroutine(byte code)
        {
            zoomCoroutineIsRunning = true;
            float fov = (code == 1) ? fovValue : wasFOVValue;
            while (_FPCamera.fieldOfView != fov)
            {
                _FPCamera.fieldOfView = Mathf.Lerp(_FPCamera.fieldOfView, fov, Time.deltaTime * smooth);
                _WeaponLayer.fieldOfView = Mathf.Lerp(_WeaponLayer.fieldOfView, fov, Time.deltaTime * smooth);
                yield return null;
            }
            zoomCoroutineIsRunning = false;
            yield break;
        }       

        public Camera FPCamera
        {
            get
            {
                return _FPCamera;
            }

            set
            {
                _FPCamera = value;
            }
        }

        public float FovValue
        {
            get
            {
                return fovValue;
            }

            set
            {
                fovValue = value;
            }
        }

        public float Speed
        {
            get
            {
                return smooth;
            }

            set
            {
                smooth = value;
            }
        }

        public float WasFOVValue
        {
            get
            {
                return wasFOVValue;
            }

            set
            {
                wasFOVValue = value;
            }
        }

        public bool ZoomCoroutineIsRunning
        {
            get
            {
                return zoomCoroutineIsRunning;
            }
        }

        public Camera WeaponLayer
        {
            get
            {
                return _WeaponLayer;
            }

            set
            {
                _WeaponLayer = value;
            }
        }
    }
}