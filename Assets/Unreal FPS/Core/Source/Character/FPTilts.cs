﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

using System.Collections;
using UnityEngine;

namespace UnrealFPS
{
    public class FPTilts : MonoBehaviour
    {
        #region Private SerializeField Variable
        [SerializeField] private float angle;
        [SerializeField] private float outputRange;
        [SerializeField] private float smooth;
        #endregion

        #region Private Variable
        private IEnumerator rotationTiltsCoroutine;
        private IEnumerator positionTiltsCoroutine;
        private NGMouseLook mouseLook;
        private Camera fpCamera;
        private Vector3 defaultCameraPosition;
        private Vector3 outputRight;
        private Vector3 outputLeft;
        #endregion

        #region [Functions]
        /// <summary>
        /// Start is called on the frame when a script is enabled just before any of the Update methods are called the first time.
        /// </summary>
        /// 
        /// <remarks>
        /// Like the Awake function, Start is called exactly once in the lifetime of the script. 
        /// However, Awake is called when the script object is initialised, regardless of whether or not the script is enabled. 
        /// Start may not be called on the same frame as Awake if the script is not enabled at initialisation time.
        /// </remarks>
        public virtual void Start()
        {
            mouseLook = GetComponent<FPController>().MouseLook;
            fpCamera = GetComponent<FPController>().Camera;
            defaultCameraPosition = fpCamera.transform.localPosition;
            outputLeft = new Vector3(-outputRange, fpCamera.transform.localPosition.y, fpCamera.transform.localPosition.z);
            outputRight = new Vector3(outputRange, fpCamera.transform.localPosition.y, fpCamera.transform.localPosition.z);
        }

        /// <summary>
        /// Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        protected virtual void Update()
        {
            short code = -1;

            if (UInput.GetButtonDown("Right Tilt"))
                code = 2;
            else if (UInput.GetButtonDown("Left Tilt"))
                code = 1;
            else if (UInput.GetButtonUp("Right Tilt") && UInput.GetButtonUp("Left Tilt"))
                code = 0;

            if (code < 0)
                return;

            if (RotationTiltsCoroutine != null)
                StopCoroutine(RotationTiltsCoroutine);
            RotationTiltsCoroutine = RotationTiltsProcessing(1);
            StartCoroutine(RotationTiltsProcessing(1));

            if (PositionTiltsCoroutine != null)
                StopCoroutine(PositionTiltsCoroutine);
            PositionTiltsCoroutine = PositionTiltsProcessing(1);
            StartCoroutine(PositionTiltsProcessing(1));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public virtual IEnumerator RotationTiltsProcessing(short code)
        {
            float timeLeft = smooth;
            while (timeLeft > 0)
            {
                if (code == 0)
                    mouseLook.m_CameraTargetRot.z = Mathf.Lerp(mouseLook.m_CameraTargetRot.z, 0, Time.time / smooth);
                else
                    mouseLook.m_CameraTargetRot.z = Mathf.Lerp(mouseLook.m_CameraTargetRot.z, (code == 2) ? -angle : angle, Time.time / smooth);
                timeLeft -= Time.deltaTime;
                yield return null;
            }
            if (timeLeft <= 0)
                yield break;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public virtual IEnumerator PositionTiltsProcessing(short code)
        {
            float timeLeft = smooth;
            while (timeLeft > 0)
            {
                if (code == 0)
                    fpCamera.transform.localPosition = Vector3.Lerp(fpCamera.transform.localPosition, defaultCameraPosition, Time.time / smooth);
                else
                    fpCamera.transform.localPosition = Vector3.Lerp(fpCamera.transform.localPosition, (code == 2) ? outputRight : outputLeft, Time.time / smooth);
                timeLeft -= Time.deltaTime;
                yield return null;
            }
            if (timeLeft <= 0)
                yield break;
        }
        #endregion

        #region [Properties]
        /// <summary>
        /// Tilts smooth
        /// </summary>
        public float Smooth
        {
            get
            {
                return smooth;
            }

            set
            {
                smooth = value;
            }
        }

        /// <summary>
        /// Tilts angle
        /// </summary>
        public float Angle
        {
            get
            {
                return angle;
            }

            set
            {
                angle = value;
            }
        }

        /// <summary>
        /// Tilts output range
        /// </summary>
        public float OutputRange
        {
            get
            {
                return outputRange;
            }

            set
            {
                outputRange = value;
            }
        }

        public NGMouseLook MouseLook
        {
            get
            {
                return mouseLook;
            }

            set
            {
                mouseLook = value;
            }
        }

        public Camera FPCamera
        {
            get
            {
                return fpCamera;
            }

            set
            {
                fpCamera = value;
            }
        }

        public Vector3 DefaultCameraPosition
        {
            get
            {
                return defaultCameraPosition;
            }

            set
            {
                defaultCameraPosition = value;
            }
        }

        public Vector3 OutputRight
        {
            get
            {
                return outputRight;
            }

            set
            {
                outputRight = value;
            }
        }

        public Vector3 OutputLeft
        {
            get
            {
                return outputLeft;
            }

            set
            {
                outputLeft = value;
            }
        }

        public IEnumerator RotationTiltsCoroutine
        {
            get
            {
                return rotationTiltsCoroutine;
            }

            set
            {
                rotationTiltsCoroutine = value;
            }
        }

        public IEnumerator PositionTiltsCoroutine
        {
            get
            {
                return positionTiltsCoroutine;
            }

            set
            {
                positionTiltsCoroutine = value;
            }
        }
        #endregion
    }
}
