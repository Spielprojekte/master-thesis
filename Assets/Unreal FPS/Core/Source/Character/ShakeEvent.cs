﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

using UnityEngine;

[System.Serializable]
public class ShakeEvent
{
    [SerializeField] private Vector3 noise;

    private float duration;
    private float timeRemaining;
    private ShakeProperties data;
    private Vector3 noiseOffset;

    public ShakeEvent(ShakeProperties data)
    {
        this.data = data;

        duration = data.duration;
        timeRemaining = duration;

        float rand = 32.0f;

        noiseOffset.x = Random.Range(0.0f, rand);
        noiseOffset.y = Random.Range(0.0f, rand);
        noiseOffset.z = Random.Range(0.0f, rand);
    }

    public void Update()
    {
        float deltaTime = Time.deltaTime;

        timeRemaining -= deltaTime;

        float noiseOffsetDelta = deltaTime * data.frequency;

        noiseOffset.x += noiseOffsetDelta;
        noiseOffset.y += noiseOffsetDelta;
        noiseOffset.z += noiseOffsetDelta;

        noise.x = Mathf.PerlinNoise(noiseOffset.x, 0.0f);
        noise.y = Mathf.PerlinNoise(noiseOffset.y, 1.0f);
        noise.z = Mathf.PerlinNoise(noiseOffset.z, 2.0f);

        noise -= Vector3.one * 0.5f;

        noise *= data.amplitude;

        float agePercent = 1.0f - (timeRemaining / duration);
        noise *= data.blendOverLifetime.Evaluate(agePercent);
    }

    public bool IsAlive
    {
        get
        {
            return timeRemaining > 0.0f;
        }
    }


    public Target target
    {
        get
        {
            return data.target;
        }
    }

    public Vector3 Noise
    {
        get
        {
            return noise;
        }

        set
        {
            noise = value;
        }
    }

    public float Duration
    {
        get
        {
            return duration;
        }

        set
        {
            duration = value;
        }
    }

    public float TimeRemaining
    {
        get
        {
            return timeRemaining;
        }

        set
        {
            timeRemaining = value;
        }
    }

    public ShakeProperties Data
    {
        get
        {
            return data;
        }

        set
        {
            data = value;
        }
    }

    public Vector3 NoiseOffset
    {
        get
        {
            return noiseOffset;
        }

        set
        {
            noiseOffset = value;
        }
    }
}
