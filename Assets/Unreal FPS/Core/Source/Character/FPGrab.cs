﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

using System.Collections;
using UnityEngine;

namespace UnrealFPS
{
    public class FPGrab : MonoBehaviour
    {
        #region [Variables are editable in the inspector]
        [SerializeField] private Transform fpCamera;
        [SerializeField] private float grabRange;
        [SerializeField] private float grabDistance;
        [SerializeField] private float heightOffset;
        [SerializeField] private float spring = 50.0f;
        [SerializeField] private float damper = 5.0f;
        [SerializeField] private float drag = 10.0f;
        [SerializeField] private float angularDrag = 5.0f;
        [SerializeField] private float distance = 0.2f;
        #endregion

        #region [Required variables]
        private GameObject grabObject;
        private IHealthCallbacks health;
        private IInventoryCallbacks inventory;
        private bool isGrabbing;
        private SpringJoint springJoint;
        #endregion

        #region [Functions]
        protected virtual void Awake()
        {
            health = GetComponent<IHealthCallbacks>();
            inventory = GetComponent<IInventoryCallbacks>();
        }

        /// <summary>
        /// Start is called on the frame when a script is enabled just before
        /// any of the Update methods is called the first time.
        /// </summary>
        protected virtual void Start()
        {
            //Add and configuring Spring Joint
            if (!springJoint)
            {
                GameObject go = new GameObject("Rigidbody Dragger");
                Rigidbody body = go.AddComponent<Rigidbody>();
                springJoint = go.AddComponent<SpringJoint>();
                body.isKinematic = true;
            }

        }

        /// <summary>
        /// Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        /// <summary>
        protected virtual void Update()
        {
            if (UInput.GetButtonDown("Grab") && !isGrabbing)
            {
                RaycastHit raycastHit;
                if (Physics.Raycast(fpCamera.position, fpCamera.forward, out raycastHit, grabRange))
                {
                    if (raycastHit.rigidbody != null && !raycastHit.rigidbody.isKinematic && grabObject != null)
                    {
                        springJoint.transform.position = raycastHit.point;
                        springJoint.anchor = Vector3.zero;
                        springJoint.spring = spring;
                        springJoint.damper = damper;
                        springJoint.maxDistance = distance;
                        springJoint.connectedBody = raycastHit.rigidbody;
                        isGrabbing = true;
                        inventory.DeactivateWeapon();
                        StartCoroutine(DragObject(grabDistance));
                    }
                }
            }
            else if ((UInput.GetButtonDown("Grab") && isGrabbing) || !health.IsAlive)
            {
                grabObject = null;
                isGrabbing = false;
            }
        }

        private IEnumerator DragObject(float distance)
        {
            var oldDrag = springJoint.connectedBody.drag;
            var oldAngularDrag = springJoint.connectedBody.angularDrag;
            springJoint.connectedBody.drag = drag;
            springJoint.connectedBody.angularDrag = angularDrag;
            while (isGrabbing)
            {
                springJoint.transform.rotation = Quaternion.LookRotation(fpCamera.transform.forward);
                springJoint.transform.position = fpCamera.transform.position + (fpCamera.transform.forward * distance) + (Vector3.up * heightOffset);
                yield return null;
            }
            if (springJoint.connectedBody)
            {
                springJoint.connectedBody.drag = oldDrag;
                springJoint.connectedBody.angularDrag = oldAngularDrag;
                springJoint.connectedBody = null;
            }
        }
        #endregion

        #region [Properties]
        public float GrabRange
        {
            get
            {
                return grabRange;
            }

            set
            {
                grabRange = value;
            }
        }

        public float GrabDistance
        {
            get
            {
                return grabDistance;
            }

            set
            {
                grabDistance = value;
            }
        }

        public float HeightOffset
        {
            get
            {
                return heightOffset;
            }

            set
            {
                heightOffset = value;
            }
        }

        public float Spring
        {
            get
            {
                return spring;
            }

            set
            {
                spring = value;
            }
        }

        public float Damper
        {
            get
            {
                return damper;
            }

            set
            {
                damper = value;
            }
        }

        public float Drag
        {
            get
            {
                return drag;
            }

            set
            {
                drag = value;
            }
        }

        public float AngularDrag
        {
            get
            {
                return angularDrag;
            }

            set
            {
                angularDrag = value;
            }
        }

        public float Distance
        {
            get
            {
                return distance;
            }

            set
            {
                distance = value;
            }
        }

        public Transform FpCamera
        {
            get
            {
                return fpCamera;
            }

            set
            {
                fpCamera = value;
            }
        }
        #endregion
    }
}