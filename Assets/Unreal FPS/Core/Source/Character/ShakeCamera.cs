﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

using System.Collections.Generic;
using UnityEngine;
using UnrealFPS.Utility;
public class ShakeCamera : Singleton<ShakeCamera>
{
    protected List<ShakeEvent> shakeEvents = new List<ShakeEvent>();
    private Vector3 cameraPosition;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    /// 
    /// <remarks>
    /// Awake is called only once during the lifetime of the script instance.
    /// Awake is always called before any Start functions.
    /// This allows you to order initialization of scripts.
    /// </remarks>
    protected virtual void Awake()
    {
        cameraPosition = transform.localPosition;
    }

    /// <summary>
    /// LateUpdate is called every frame, if the Behaviour is enabled.
    /// </summary>
    /// 
    /// <remarks>
    /// LateUpdate is called every frame, if the Behaviour is enabled.
    /// Late update because it tracks objects that might have moved inside Update.
    /// </remarks>
    protected virtual void LateUpdate()
    {
        if (shakeEvents.Count == 0)
            return;

        Vector3 positionOffset = cameraPosition;
        Vector3 rotationOffset = Vector3.zero;

        for (int i = shakeEvents.Count - 1; i != -1; i--)
        {
            ShakeEvent se = shakeEvents[i];
            se.Update();

            if (se.target == Target.Position)
                positionOffset += se.Noise;
            else
                rotationOffset += se.Noise;

            if (!se.IsAlive)
                shakeEvents.RemoveAt(i);
            transform.localPosition = positionOffset;
        }

    }

    public void AddShakeEvent(ShakeProperties data)
    {
        shakeEvents.Add(new ShakeEvent(data));
    }

    public void AddShakeEvent(ShakeEvent shakeEvent)
    {
        shakeEvents.Add(shakeEvent);
    }
    

    public static ShakeProperties HitShakeProperties()
    {
        return new ShakeProperties(0.1f, 5, 0.5f, new AnimationCurve(
            new Keyframe(0.0f, 0.0f, Mathf.Deg2Rad * 0.0f, Mathf.Deg2Rad * 720.0f),
            new Keyframe(0.2f, 1.0f),
            new Keyframe(1.0f, 0.0f)), Target.Position);

    }

    public static ShakeProperties ExplosionShakeProperties()
    {
        return new ShakeProperties(0.5f, 5, 0.5f, new AnimationCurve(
            new Keyframe(0.0f, 0.0f, Mathf.Deg2Rad * 0.0f, Mathf.Deg2Rad * 720.0f),
            new Keyframe(0.2f, 1.0f),
            new Keyframe(1.0f, 0.0f)), Target.Position);
    }

    public static ShakeProperties ExplosionShakeProperties(float radius, float distance, float minAmplitude = 0, float maxAmplitude = 0.55f)
    {
        float amplitude = Mathf.InverseLerp(0, radius, radius - distance);
        amplitude = Mathf.Clamp(amplitude, minAmplitude, maxAmplitude);
        return new ShakeProperties(amplitude, 5, 0.5f, new AnimationCurve(
            new Keyframe(0.0f, 0.0f, Mathf.Deg2Rad * 0.0f, Mathf.Deg2Rad * 720.0f),
            new Keyframe(0.2f, 1.0f),
            new Keyframe(1.0f, 0.0f)), Target.Position);
    }
}