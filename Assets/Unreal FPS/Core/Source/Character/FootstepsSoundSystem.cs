﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;
using Object = UnityEngine.Object;

namespace UnrealFPS
{
    [Serializable]
    public struct FootstepParam
    {
        public string name;
        public PhysicMaterial physicMaterial;
        public Texture2D texture;
        public AudioClip[] FootStepsSound;
    }

    /// <summary>
    /// 
    /// </summary>
    public sealed class FootstepsSoundSystem
    {
        /// <summary>
        /// Handle sound by ray
        /// </summary>
        /// <param name="player"></param>
        /// <param name="audioSource"></param>
        public static void Play(List<FootstepParam> footstepParam, Transform player, AudioSource audioSource)
        {
            RaycastHit footstepshit;
            if (Physics.Raycast(player.position, Vector3.down, out footstepshit))
            {
                Object surfaceInfo = SurfaceHelper.GetSurfaceType(footstepshit.collider, player.position);
                if (!surfaceInfo)
                    return;

                for (int i = 0; i < footstepParam.Count; i++)
                {
                    if (footstepParam[i].physicMaterial == surfaceInfo || footstepParam[i].texture == surfaceInfo)
                    {
                        if (footstepParam[i].FootStepsSound.Length > 0)
                        {
                            int randomSound = Random.Range(0, footstepParam[i].FootStepsSound.Length);
                            audioSource.PlayOneShot(footstepParam[i].FootStepsSound[randomSound]);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Handle sound by surface name
        /// </summary>
        /// <param name="surfaceName"></param>
        /// <param name="audioSource"></param>
        public static void Play(List<FootstepParam> footstepParam, Collider collider, AudioSource audioSource)
        {
            Object surfaceInfo = SurfaceHelper.GetSurfaceType(collider, audioSource.transform.root.position);
            if (!surfaceInfo)
                return;

            for (int i = 0; i < footstepParam.Count; i++)
            {
                if (footstepParam[i].physicMaterial == surfaceInfo || footstepParam[i].texture == surfaceInfo)
                {
                    if (footstepParam[i].FootStepsSound.Length > 0)
                    {
                        int randomSound = Random.Range(0, footstepParam[i].FootStepsSound.Length);
                        audioSource.PlayOneShot(footstepParam[i].FootStepsSound[randomSound]);
                    }
                }
            }
        }
    }
}
