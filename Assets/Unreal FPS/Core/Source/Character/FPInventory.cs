﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnrealFPS.UI;
using UnrealFPS.Utility;

namespace UnrealFPS
{
    [Serializable]
    public struct InventorySlot
    {
        public KeyCode key;
        public Weapon weapon;
    }

    [Serializable]
    public struct InventoryGroup
    {
        public string name;
        public InventorySlot[] inventorySlots;
    }

    public class FPInventory : MonoBehaviour, IInventoryCallbacks
    {
        [SerializeField] private Transform _FPSCamera;
        [SerializeField] private bool allowMultipleIdenticalWeapons;
        [SerializeField] private float dropThrowForce;
        [SerializeField] private InventoryGroup[] inventoryGroups;

        private Transform activeWeapon;
        private Dictionary<KeyCode, int[]> cacheKeyData;
        private bool weaponAnimationCoroutineIsRunning;
        private bool weaponDropCoroutineIsRunning;
        private int activeGroupIndex;
        private int activeWeaponIndex;



        #region [Functions]
        /// <summary>
        /// Start is called on the frame when a script is enabled just before any of the Update methods are called the first time.
        /// </summary>
        /// 
        /// <remarks>
        /// Like the Awake function, Start is called exactly once in the lifetime of the script. 
        /// However, Awake is called when the script object is initialised, regardless of whether or not the script is enabled. 
        /// Start may not be called on the same frame as Awake if the script is not enabled at initialisation time.
        /// </remarks>
        protected virtual void Start()
        {
            FindStartWeapon();
            FillCacheKeyData(ref cacheKeyData, inventoryGroups);
        }

        /// <summary>
        /// Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        protected virtual void Update()
        {
            SelectWeaponByKey();
            SelectWeaponByMouseWheel();

            if (UInput.GetButtonDown("DropWeapon"))
                DropWeapon();
        }

        /// <summary>
        /// Select weapon by specific keycode.
        /// </summary>
        protected virtual void SelectWeaponByKey()
        {
            if (!Input.anyKeyDown || weaponAnimationCoroutineIsRunning)
                return;

            KeyCode keyCode = Extension.GetPressedKey();
            if (!cacheKeyData.ContainsKey(keyCode))
                return;

            Weapon nextWeapon = inventoryGroups[cacheKeyData[keyCode][0]].inventorySlots[cacheKeyData[keyCode][1]].weapon;
            Weapon currentWeapon = (activeWeapon != null) ? activeWeapon.GetComponent<WeaponIdentifier>().Weapon : null;
            if (nextWeapon == null || nextWeapon == currentWeapon)
                return;

            string id = nextWeapon.Id;
            ActivateWeapon(id);
            Weapon weaponInstance = activeWeapon.GetComponent<WeaponIdentifier>().Weapon;
            GameUIManager.Instance._HUDManager.WeaponProcessing(weaponInstance.DisplayName, weaponInstance.Image);
        }

        /// <summary>
        /// Select weapon by mouse wheel
        /// </summary>
        protected virtual void SelectWeaponByMouseWheel()
        {
            if (UInput.GetAxis("Mouse Wheel") == 0 || weaponAnimationCoroutineIsRunning)
                return;

            Transform weapon;
            Weapon weaponInstance;
            float sign = Mathf.Sign(UInput.GetAxis("Mouse Wheel"));

            if (sign > 0)
                weapon = GetNextWeapon();
            else
                weapon = GetPreviousWeapon();
            if (weapon == null)
                return;

            weaponInstance = weapon.GetComponent<WeaponIdentifier>().Weapon;
            ActivateWeapon(weaponInstance.Id);
            weaponInstance = activeWeapon.GetComponent<WeaponIdentifier>().Weapon;
            GameUIManager.Instance._HUDManager.WeaponProcessing(weaponInstance.DisplayName, weaponInstance.Image);
        }

        /// <summary>
		/// Add new weapon and take it.
		/// </summary>
		/// <param name="weapon"></param>
        public virtual void AddWeapon(Weapon weapon)
        {
            if (!allowMultipleIdenticalWeapons)
            {
                for (int i = 0; i < inventoryGroups.Length; i++)
                {
                    for (int j = 0; j < inventoryGroups[i].inventorySlots.Length; j++)
                    {
                        if (inventoryGroups[i].inventorySlots[j].weapon == weapon)
                            return;
                        else
                            continue;
                    }
                }
            }

            for (int i = 0; i < inventoryGroups.Length; i++)
            {
                if (weapon.Group == inventoryGroups[i].name)
                {
                    for (int j = 0; j < inventoryGroups[i].inventorySlots.Length; j++)
                    {
                        if (inventoryGroups[i].inventorySlots[j].weapon == null)
                        {
                            inventoryGroups[i].inventorySlots[j].weapon = weapon;
                            ActivateWeapon(weapon.Id);
                            return;
                        }
                        else if (j == inventoryGroups[i].inventorySlots.Length - 1)
                        {
                            if (inventoryGroups[i].inventorySlots[j].weapon != null)
                                DropWeapon(inventoryGroups[i].inventorySlots[j].weapon.Id);
                            inventoryGroups[i].inventorySlots[j].weapon = weapon;
                            ActivateWeapon(weapon.Id);
                            return;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Drop active weapon.
        /// </summary>
        public virtual void DropWeapon()
        {
            if (activeWeapon == null)
                return;

            string id = activeWeapon.GetComponent<WeaponIdentifier>().Weapon.Id;
            DropWeaponProcessing(id);
        }

        /// <summary>
        /// Drop specific weapon.
        /// </summary>
        /// <param name="id">ID form WeaponIdentifier component</param>
        public virtual void DropWeapon(string id)
        {
            DropWeaponProcessing(id);
        }

        /// <summary>
		/// Activate weapon by id.
		/// </summary>
		/// <param name="id">ID form WeaponIdentifier component</param>
        public virtual void ActivateWeapon(string id)
        {
            //Get reqired data about weapon
            Transform weapon = GetWeapon(id);
            Weapon weaponInstance = weapon.GetComponent<WeaponIdentifier>().Weapon;
            int[] indexes = GetIndexes(weaponInstance.Id, weaponInstance.Group);

            WeaponAnimationProcessing(activeWeapon, weapon);

            activeWeapon = weapon;
            activeGroupIndex = indexes[0];
            activeWeaponIndex = indexes[1];
        }

        /// <summary>
        /// Activate next weapon in inventory.
        /// If active weapon last in inventory, will activate first weapon from inventory.
        /// </summary>
        public virtual void ActivateNextWeapon()
        {
            string id = "None";
            if (activeWeaponIndex < inventoryGroups[activeGroupIndex].inventorySlots.Length - 1)
            {
                id = inventoryGroups[activeGroupIndex].inventorySlots[activeWeaponIndex + 1].weapon.Id;
                activeWeaponIndex = activeWeaponIndex + 1;
            }
            else if (activeGroupIndex < inventoryGroups.Length - 1)
            {
                id = inventoryGroups[activeGroupIndex + 1].inventorySlots[0].weapon.Id;
                activeGroupIndex = activeGroupIndex + 1;
                activeWeaponIndex = 0;
            }
            else
            {
                id = inventoryGroups[0].inventorySlots[0].weapon.Id;
                activeGroupIndex = 0;
                activeWeaponIndex = 0;
            }
            Transform weapon = GetWeapon(id);
            WeaponAnimationProcessing(activeWeapon, weapon);
            activeWeapon = weapon;
        }

        /// <summary>
        /// Activate previous weapon in inventory.
        /// If active weapon is first in inventory, will activate last weapon from inventory.
        /// </summary>
        public virtual void ActivatePreviousWeapon()
        {
            string id = "None";
            if (activeWeaponIndex - 1 >= 0)
            {
                id = inventoryGroups[activeGroupIndex].inventorySlots[activeWeaponIndex - 1].weapon.Id;
                activeWeaponIndex = activeWeaponIndex - 1;
            }
            else if (activeGroupIndex - 1 > 0)
            {
                id = inventoryGroups[activeGroupIndex - 1].inventorySlots[inventoryGroups[activeGroupIndex - 1].inventorySlots.Length - 1].weapon.Id;
                activeGroupIndex = activeGroupIndex - 1;
                activeWeaponIndex = activeGroupIndex - 1;
            }
            else
            {
                id = inventoryGroups[inventoryGroups.Length - 1].inventorySlots[inventoryGroups[activeGroupIndex - 1].inventorySlots.Length - 1].weapon.Id;
                activeGroupIndex = inventoryGroups.Length - 1;
                activeWeaponIndex = inventoryGroups[activeGroupIndex - 1].inventorySlots.Length - 1;
            }
            Transform weapon = GetWeapon(id);
            WeaponAnimationProcessing(activeWeapon, weapon);
            activeWeapon = weapon;
        }

        /// <summary>
        /// Activate deactivated weapon.
        /// </summary>
        public void ActivateLastWeapon()
        {
            WeaponAnimationProcessing(null, activeWeapon);
        }

        /// <summary>
        /// Deactivate current active weapon.
        /// </summary>
        public virtual void DeactivateWeapon()
        {
            if (activeWeapon != null)
                WeaponAnimationProcessing(activeWeapon, null);
        }

        /// <summary>
        /// Deativate weapon by id.
        /// </summary>
        /// <param name="id">ID form WeaponIdentifier component</param>
        public virtual void DeactivateWeapon(string id)
        {
            if (GetWeapon(id) != null)
                WeaponAnimationProcessing(GetWeapon(id), null);
        }

        /// <summary>
		/// Get current active weapon.
		/// </summary>
		/// <returns>Transform</returns>
        public virtual Transform GetActiveWeapon()
        {
            return activeWeapon;
        }

        /// <summary>
        /// Get weapon by index from FPCamera childs.
        /// </summary>
        /// <param name="index">Index from FPCamera childs</param>
        /// <returns>Transform: By specific index</returns>
        public Transform GetWeapon(int index)
        {
            if (index > _FPSCamera.childCount)
                return null;

            if (_FPSCamera.GetChild(index) != null)
                return _FPSCamera.GetChild(index);

            return null;
        }

        /// <summary>
        /// Get weapon by id from FPCamera childs.
        /// </summary>
        /// <param name="id">ID form WeaponIdentifier component</param>
        /// <returns>Transform: By specific id</returns>
        public Transform GetWeapon(string id)
        {
            WeaponIdentifier[] weapons = _FPSCamera.GetComponentsInChildren<WeaponIdentifier>(true);
            for (int i = 0; i < weapons.Length; i++)
                if (weapons[i].Weapon.Id == id)
                    return weapons[i].transform;
            return null;
        }

        /// <summary>
        /// Return next weapon in inventory.
        /// If active weapon last in inventory, will return first weapon from inventory.
        /// </summary>
        /// <returns>Transform: next weapon</returns>
        public Transform GetNextWeapon()
        {
            Transform weapon;
            string id = "None";
            if (activeWeaponIndex < inventoryGroups[activeGroupIndex].inventorySlots.Length - 1)
            {
                if (inventoryGroups[activeGroupIndex].inventorySlots[activeWeaponIndex + 1].weapon != null)
                {
                    id = inventoryGroups[activeGroupIndex].inventorySlots[activeWeaponIndex + 1].weapon.Id;
                    activeWeaponIndex = activeWeaponIndex + 1;
                }
            }
            else if (activeGroupIndex < inventoryGroups.Length - 1)
            {
                if (inventoryGroups[activeGroupIndex + 1].inventorySlots[0].weapon != null)
                {
                    id = inventoryGroups[activeGroupIndex + 1].inventorySlots[0].weapon.Id;
                    activeGroupIndex = activeGroupIndex + 1;
                    activeWeaponIndex = 0;
                }
            }
            else
            {
                if (inventoryGroups[0].inventorySlots[0].weapon != null)
                {
                    id = inventoryGroups[0].inventorySlots[0].weapon.Id;
                    activeGroupIndex = 0;
                    activeWeaponIndex = 0;
                }
            }
            weapon = GetWeapon(id);
            return weapon;
        }

        /// <summary>
        /// Return previous weapon in inventory.
        /// If active weapon is first in inventory, will return last weapon from inventory.
        /// </summary>
        /// <returns>Transform: previous weapon</returns>
        public Transform GetPreviousWeapon()
        {
            Transform weapon;
            string id = "None";
            if (activeWeaponIndex - 1 >= 0)
            {
                if (inventoryGroups[activeGroupIndex].inventorySlots[activeWeaponIndex - 1].weapon != null)
                {
                    id = inventoryGroups[activeGroupIndex].inventorySlots[activeWeaponIndex - 1].weapon.Id;
                    activeWeaponIndex = activeWeaponIndex - 1;
                }
            }
            else if (activeGroupIndex - 1 >= 0)
            {
                if (inventoryGroups[activeGroupIndex - 1].inventorySlots[inventoryGroups[activeGroupIndex - 1].inventorySlots.Length - 1].weapon != null)
                {
                    id = inventoryGroups[activeGroupIndex - 1].inventorySlots[inventoryGroups[activeGroupIndex - 1].inventorySlots.Length - 1].weapon.Id;
                    activeGroupIndex = activeGroupIndex - 1;
                    activeWeaponIndex = activeGroupIndex - 1;
                }
            }
            else
            {
                if (inventoryGroups[inventoryGroups.Length - 1].inventorySlots[inventoryGroups[inventoryGroups.Length - 1].inventorySlots.Length - 1].weapon != null)
                {
                    id = inventoryGroups[inventoryGroups.Length - 1].inventorySlots[inventoryGroups[inventoryGroups.Length - 1].inventorySlots.Length - 1].weapon.Id;
                    activeGroupIndex = inventoryGroups.Length - 1;
                    activeWeaponIndex = inventoryGroups[activeGroupIndex - 1].inventorySlots.Length - 1;
                }
            }
            weapon = GetWeapon(id);
            return weapon;
        }

        /// <summary>
        /// Get group index and weapon index from inventory.
        ///     0 element - Group index.
        ///     1 element - Weapon index.
        /// 
        /// If weapon null or not exists return first group and first weapon indexes.
        /// </summary>
        /// <returns></returns>
        public int[] GetIndexes(string id, string group)
        {
            for (int i = 0; i < inventoryGroups.Length; i++)
            {
                if (inventoryGroups[i].name == group)
                {
                    for (int j = 0; j < inventoryGroups[i].inventorySlots.Length; j++)
                    {
                        if (inventoryGroups[i].inventorySlots[j].weapon != null && inventoryGroups[i].inventorySlots[j].weapon.Id == id)
                        {
                            return new int[2] { i, j };
                        }
                    }
                }
            }
            return new int[2] { 0, 0 };
        }

        /// <summary>
        /// Get group index and weapon index from inventory.
        ///     0 element - Group index.
        ///     1 element - Weapon index.
        /// 
        /// If weapon null or not exists return first group and first weapon indexes.
        /// </summary>
        /// <returns></returns>
        public int[] GetIndexes(string id)
        {
            for (int i = 0; i < inventoryGroups.Length; i++)
            {
                for (int j = 0; j < inventoryGroups[i].inventorySlots.Length; j++)
                {
                    if (inventoryGroups[i].inventorySlots[j].weapon.Id == id)
                    {
                        return new int[2] { i, j };
                    }
                }
            }
            return new int[2] { 0, 0 };
        }

        /// <summary>
        /// Find and save first active weapon on start.
        /// All next's weapon will disabled.
        /// </summary>
        protected virtual void FindStartWeapon()
        {
            for (int i = 0; i < _FPSCamera.childCount; i++)
            {
                if (activeWeapon == null && _FPSCamera.GetChild(i).gameObject.activeSelf && _FPSCamera.GetChild(i).CompareTag("Weapon"))
                {
                    activeWeapon = _FPSCamera.GetChild(i);
                    Weapon weaponInstance = activeWeapon.GetComponent<WeaponIdentifier>().Weapon;
                    int[] indexes = GetIndexes(weaponInstance.Id, weaponInstance.Group);
                    activeGroupIndex = indexes[0];
                    activeWeaponIndex = indexes[1];
                    continue;
                }
                if (_FPSCamera.GetChild(i).CompareTag("Weapon"))
                    _FPSCamera.GetChild(i).gameObject.SetActive(false);
            }
            if(activeWeapon != null)
            {
                Weapon weaponInstance = activeWeapon.GetComponent<WeaponIdentifier>().Weapon;
                GameUIManager.Instance._HUDManager.WeaponProcessing(weaponInstance.DisplayName, weaponInstance.Image);
            }
        }

        /// <summary>
        /// Initialize new inventory group.
        /// </summary>
        /// <param name="inventoryGroup"></param>
        public virtual void InitializeInventoryGroup(InventoryGroup[] inventoryGroups)
        {
            activeGroupIndex = 0;
            activeWeaponIndex = 0;
            this.inventoryGroups = inventoryGroups;
            FillCacheKeyData(ref cacheKeyData, inventoryGroups);
        }

        /// <summary>
        /// Fill cache key data by inventory group.
        /// </summary>
        /// <param name="cacheKeyData"></param>
        protected virtual void FillCacheKeyData(ref Dictionary<KeyCode, int[]> cacheKeyData, InventoryGroup[] inventoryGroups)
        {
            cacheKeyData = new Dictionary<KeyCode, int[]>();
            for (int i = 0; i < inventoryGroups.Length; i++)
            {
                for (int j = 0; j < inventoryGroups[i].inventorySlots.Length; j++)
                {
                    cacheKeyData.Add(inventoryGroups[i].inventorySlots[j].key, new int[2] { i, j });
                }
            }
        }

        /// <summary>
        /// Processing weapon animation, put away current weapon and take new weapon.
        /// </summary>
        /// <param name="currentWeapon"></param>
        /// <param name="newWeapon"></param>
        public virtual void WeaponAnimationProcessing(Transform currentWeapon, Transform newWeapon)
        {
            if (weaponAnimationCoroutineIsRunning)
                return;

            StartCoroutine(WeaponAnimationProcessingCoroutine(currentWeapon, newWeapon));
        }

        /// <summary>
        /// Drop weapon processing, put away current weapon, drop current weapon and take next weapon.
        /// </summary>
        /// <param name="id">ID form WeaponIdentifier component</param>
        /// <returns>IEnumerator</returns>
        public virtual void DropWeaponProcessing(string id)
        {
            if (weaponDropCoroutineIsRunning)
                return;

            StartCoroutine(DropWeaponProcessingCoroutine(id));
        }

        /// <summary>
        /// Processing weapon animation, put away current weapon, take new weapon.
        /// </summary>
        /// <param name="currentWeapon"></param>
        /// <param name="newWeapon"></param>
        /// <returns>IEnumerator</returns>
        protected virtual IEnumerator WeaponAnimationProcessingCoroutine(Transform currentWeapon, Transform newWeapon)
        {
            weaponAnimationCoroutineIsRunning = true;
            if (currentWeapon != null && currentWeapon.gameObject.activeSelf)
            {
                IWeaponAnimatorCallbacks weaponAnimatorCallbacks = currentWeapon.GetComponent<IWeaponAnimatorCallbacks>();
                if (weaponAnimatorCallbacks != null)
                {
                    weaponAnimatorCallbacks.PutAway();
                    yield return new WaitForSeconds(weaponAnimatorCallbacks.GetPutAwayTime());
                    currentWeapon.gameObject.SetActive(false);
                }
                else
                {
                    currentWeapon.gameObject.SetActive(false);
                }
            }
            if (newWeapon != null)
                newWeapon.gameObject.SetActive(true);
            weaponAnimationCoroutineIsRunning = false;
            yield break;
        }

        /// <summary>
        /// Drop weapon processing, put away current weapon, drop current weapon and take next weapon.
        /// </summary>
        /// <param name="id">ID form WeaponIdentifier component</param>
        /// <returns>IEnumerator</returns>
        protected virtual IEnumerator DropWeaponProcessingCoroutine(string id)
        {
            weaponDropCoroutineIsRunning = true;
            Transform dropWeapon = GetWeapon(id).GetComponent<WeaponIdentifier>().Weapon.Drop.transform;
            inventoryGroups[activeGroupIndex].inventorySlots[activeWeaponIndex].weapon = null;
            IWeaponAnimatorCallbacks weaponAnimatorCallbacks = (activeWeapon != null) ? activeWeapon.GetComponent<IWeaponAnimatorCallbacks>() : null;
            if (weaponAnimatorCallbacks != null)
            {
                weaponAnimatorCallbacks.PutAway();
                yield return new WaitForSeconds(weaponAnimatorCallbacks.GetPutAwayTime());
                activeWeapon.gameObject.SetActive(false);
            }
                GameObject cloneDropWeapon = Instantiate(dropWeapon.gameObject, transform.position + transform.forward * 0.5f, Quaternion.identity);
            Rigidbody rigidbody = cloneDropWeapon.GetComponent<Rigidbody>();
            if (rigidbody != null)
                rigidbody.AddForce(transform.forward * dropThrowForce, ForceMode.Impulse);
            weaponDropCoroutineIsRunning = false;
            yield break;
        }
        #endregion

        #region [Properties]
        public InventoryGroup[] InventoryGroups
        {
            get
            {
                return inventoryGroups;
            }

            protected set
            {
                inventoryGroups = value;
            }
        }

        public int ActiveGroupIndex
        {
            get
            {
                return activeGroupIndex;
            }

            protected set
            {
                activeGroupIndex = value;
            }
        }

        public int ActiveWeaponIndex
        {
            get
            {
                return activeWeaponIndex;
            }

            protected set
            {
                activeWeaponIndex = value;
            }
        }

        public Dictionary<KeyCode, int[]> CacheKeyData
        {
            get
            {
                return cacheKeyData;
            }

            protected set
            {
                cacheKeyData = value;
            }
        }

        public Transform FPSCamera
        {
            get
            {
                return _FPSCamera;
            }

            set
            {
                _FPSCamera = value;
            }
        }

        public bool AllowMultipleIdenticalWeapons
        {
            get
            {
                return allowMultipleIdenticalWeapons;
            }

            set
            {
                allowMultipleIdenticalWeapons = value;
            }
        }

        public float DropThrowForce
        {
            get
            {
                return dropThrowForce;
            }

            set
            {
                dropThrowForce = value;
            }
        }
        #endregion
    }
}