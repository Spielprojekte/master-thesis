﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

using UnityEngine;

namespace UnrealFPS.Utility
{
    public class UPoolObject : MonoBehaviour, IPoolObject<string>
    {
        public virtual string Group { get { return name; } }
        public Transform MyTransform { get { return myTransform; } }

        protected Transform myTransform;

        /// <summary>
        /// Awake is called when the script instance is being loaded.
        /// </summary>
        /// 
        /// <remarks>
        /// Awake is called only once during the lifetime of the script instance.
        /// Awake is always called before any Start functions.
        /// This allows you to order initialization of scripts.
        /// </remarks>
        protected virtual void Awake()
        {
            myTransform = transform;
        }

        /// <summary>
        /// Set position and rotation for transform
        /// </summary>
        /// <param name="position">Vector3 position</param>
        /// <param name="rotation">Quaternion rotation</param>
        public virtual void SetTransform(Vector3 position, Quaternion rotation)
        {
            myTransform.position = position;
            myTransform.rotation = rotation;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// 
        /// <remarks>
        /// Constructor for pool
        /// </remarks>
        public virtual void Create()
        {
            gameObject.SetActive(true);
        }

        /// <summary>
        /// Destructor
        /// </summary>
        /// 
        /// <remarks>
        /// Destructor for pool
        /// </remarks>
        public virtual void OnPush()
        {
            gameObject.SetActive(false);
        }

        /// <summary>
        /// Call destructor
        /// </summary>
        public virtual void Push()
        {
            UPoolManager.Instance.Push(Group, this);
        }

        /// <summary>
        /// Failed to add in pool 
        /// </summary>
        public void FailedPush()
        {
            Debug.Log("FailedPush");
            Destroy(gameObject);
        }
    }
}