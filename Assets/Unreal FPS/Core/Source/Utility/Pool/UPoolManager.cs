﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

using UnityEngine;

namespace UnrealFPS.Utility
{
    public class UPoolManager : Singleton<UPoolManager>
    {
        [SerializeField] private int maxInstanceCount = 128;

        protected PoolManager<string, UPoolObject> poolManager;

        /// <summary>
        /// Awake is called when the script instance is being loaded.
        /// </summary>
        /// 
        /// <remarks>
        /// Awake is called only once during the lifetime of the script instance.
        /// Awake is always called before any Start functions.
        /// This allows you to order initialization of scripts.
        /// </remarks>
        protected virtual void Awake()
        {
            poolManager = new PoolManager<string, UPoolObject>(maxInstanceCount);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="groupKey"></param>
        /// <param name="poolObject"></param>
        /// <returns></returns>
        public virtual bool Push(string groupKey, UPoolObject poolObject)
        {
            return poolManager.Push(groupKey, poolObject);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="prefab"></param>
        /// <returns></returns>
        public virtual T PopOrCreate<T>(T prefab) where T : UPoolObject
        {
            return PopOrCreate(prefab, Vector3.zero, Quaternion.identity);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="prefab"></param>
        /// <param name="position"></param>
        /// <param name="rotation"></param>
        /// <returns></returns>
        public virtual T PopOrCreate<T>(T prefab, Vector3 position, Quaternion rotation) where T : UPoolObject
        {
            T result = poolManager.Pop<T>(prefab.Group);
            if (result == null)
            {
                result = CreateObject<T>(prefab, position, rotation);
            }
            else
            {
                result.SetTransform(position, rotation);
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="groupKey"></param>
        /// <returns></returns>
        public virtual UPoolObject Pop(string groupKey)
        {
            return poolManager.Pop<UPoolObject>(groupKey);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public virtual T Pop<T>() where T : UPoolObject
        {
            return poolManager.Pop<T>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="comparer"></param>
        /// <returns></returns>
        public virtual T Pop<T>(PoolManager<string, UPoolObject>.Compare<T> comparer) where T : UPoolObject
        {
            return poolManager.Pop<T>(comparer);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="groupKey"></param>
        /// <returns></returns>
        public virtual T Pop<T>(string groupKey) where T : UPoolObject
        {
            return poolManager.Pop<T>(groupKey);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="groupKey"></param>
        /// <returns></returns>
        public virtual bool Contains(string groupKey)
        {
            return poolManager.Contains(groupKey);
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual void Clear()
        {
            poolManager.Clear();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="prefab"></param>
        /// <param name="position"></param>
        /// <param name="rotation"></param>
        /// <returns></returns>
        protected virtual T CreateObject<T>(T prefab, Vector3 position, Quaternion rotation) where T : UPoolObject
        {
            GameObject go = Instantiate(prefab.gameObject, position, rotation) as GameObject;
            T result = go.GetComponent<T>();
            result.name = prefab.name;
            return result;
        }

        public bool CanPush { get { return poolManager.CanPush; } }

        public int MaxInstanceCount { get { return maxInstanceCount; } set { maxInstanceCount = value; } }
    }
}