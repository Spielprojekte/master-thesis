﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

namespace UnrealFPS.Utility
{
    public interface IPoolObject<T>
    {
        /// <summary>
        /// Group identifire
        /// </summary>
        T Group { get; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// 
        /// <remarks>
        /// Constructor for pool
        /// </remarks>
        void Create();

        /// <summary>
        /// Destructor
        /// </summary>
        /// 
        /// <remarks>
        /// Destructor for pool
        /// </remarks>
        void OnPush();

        /// <summary>
        /// Failed to add in pool 
        /// </summary>
        void FailedPush();
    }
}