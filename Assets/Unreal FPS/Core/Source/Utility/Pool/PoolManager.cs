﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Unreal FPS
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Favilevich
   ---------------------------------------------------
   Copyright © Tamerlan Favilevich 2017 - 2018 All rights reserved.
   ================================================================ */

using System.Collections.Generic;
using System;

namespace UnrealFPS.Utility
{
    public class PoolManager<K, V> where V : IPoolObject<K>
    {
        public delegate bool Compare<T>(T value) where T : V;

        private int maxInstances;

        protected Dictionary<K, List<V>> objects;
        protected Dictionary<Type, List<V>> cache;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="maxInstance"></param>
        public PoolManager(int maxInstance)
        {
            this.maxInstances = maxInstance;
            objects = new Dictionary<K, List<V>>();
            cache = new Dictionary<Type, List<V>>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="groupKey">Group key</param>
        /// <param name="value">IPoolObject<K></param>
        /// <returns></returns>
        public virtual bool Push(K groupKey, V value)
        {
            bool result = false;
            if (CanPush)
            {
                value.OnPush();
                if (!objects.ContainsKey(groupKey))
                {
                    objects.Add(groupKey, new List<V>());
                }
                objects[groupKey].Add(value);
                Type type = value.GetType();
                if (!cache.ContainsKey(type))
                {
                    cache.Add(type, new List<V>());
                }
                cache[type].Add(value);
            }
            else
            {
                value.FailedPush();
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T">IPoolObject<K></typeparam>
        /// <param name="groupKey">Group key</param>
        /// <returns></returns>
        public virtual T Pop<T>(K groupKey) where T : V
        {
            T result = default(T);
            if (Contains(groupKey) && objects[groupKey].Count > 0)
            {
                for (int i = 0; i < objects[groupKey].Count; i++)
                {
                    if (objects[groupKey][i] is T)
                    {
                        result = (T)objects[groupKey][i];
                        Type type = result.GetType();
                        RemoveObject(groupKey, i);
                        RemoveFromCache(result, type);
                        result.Create();
                        break;
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T">IPoolObject<K></typeparam>
        /// <returns></returns>
        public virtual T Pop<T>() where T : V
        {
            T result = default(T);
            Type type = typeof(T);
            if (ValidateForPop(type))
            {
                for (int i = 0; i < cache[type].Count; i++)
                {
                    result = (T)cache[type][i];
                    if (result != null && objects.ContainsKey(result.Group))
                    {
                        objects[result.Group].Remove(result);
                        RemoveFromCache(result, type);
                        result.Create();
                        break;
                    }

                }
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T">IPoolObject<K></typeparam>
        /// <param name="comparer"></param>
        /// <returns></returns>
        public virtual T Pop<T>(Compare<T> comparer) where T : V
        {
            T result = default(T);
            Type type = typeof(T);
            if (ValidateForPop(type))
            {
                for (int i = 0; i < cache[type].Count; i++)
                {
                    T value = (T)cache[type][i];
                    if (comparer(value))
                    {
                        objects[value.Group].Remove(value);
                        RemoveFromCache(result, type);
                        result = value;
                        result.Create();
                        break;
                    }

                }
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="groupKey"></param>
        /// <returns></returns>
        public virtual bool Contains(K groupKey)
        {
            return objects.ContainsKey(groupKey);
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual void Clear()
        {
            objects.Clear();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        protected virtual bool ValidateForPop(Type type)
        {
            return cache.ContainsKey(type) && cache[type].Count > 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="groupKey"></param>
        /// <param name="idx"></param>
        protected virtual void RemoveObject(K groupKey, int idx)
        {
            if (idx >= 0 && idx < objects[groupKey].Count)
            {
                objects[groupKey].RemoveAt(idx);
                if (objects[groupKey].Count == 0)
                {
                    objects.Remove(groupKey);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="type"></param>
        protected void RemoveFromCache(V value, Type type)
        {
            if (cache.ContainsKey(type))
            {
                cache[type].Remove(value);
                if (cache[type].Count == 0)
                {
                    cache.Remove(type);
                }
            }
        }

        public int MaxInstances { get { return maxInstances; } protected set { maxInstances = value; } }

        public int InctanceCount { get { return objects.Count; } }

        public int CacheCount { get { return cache.Count; } }

        public bool CanPush { get { return InctanceCount + 1 < maxInstances; } }
    }
}